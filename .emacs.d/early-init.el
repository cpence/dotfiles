;; Move the eln-cache location
(startup-redirect-eln-cache (expand-file-name "~/.cache/emacs/eln-cache"))

;; Move the package cache location
(setq package-user-dir (expand-file-name "~/.cache/emacs/elpa"))
