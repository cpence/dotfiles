;;; svmono.el --- AUCTeX style for `svmono.csl'

(TeX-add-style-hook
 "svmono"
 (lambda ()
   (TeX-run-style-hooks "natbib" "geometry" "graphicx" "ntheorem" "color" "framed" "xcolor" )))
