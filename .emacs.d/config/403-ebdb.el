(setq package-selected-packages (cons 'ebdb package-selected-packages))
(use-package ebdb
  :ensure t
  :bind (("<f5>" . cp-open-ebdb))
  :custom
  (ebdb-sources (expand-file-name "~/Nextcloud/Contacts/Contacts.ebdb"))
  (ebdb-save-on-exit t)

  ;; Show me records, but don't update automatically
  (ebdb-mua-pop-up t)
  (ebdb-mua-auto-update-p nil)

  ;; Let me customize names fully
  (ebdb-read-name-articulate t)
  :config
  ;; Load VCard exporter (for below)
  (require 'ebdb-vcard)

  ;; Create link type for EBDB records in Org
  (require 'ebdb-org)

  (defun cp-open-ebdb ()
    (interactive)
    (ebdb 'display ".*")))

(use-package ebdb-com
  :after (ebdb)
  :bind (:map ebdb-mode-map
              ("X" . cp-export-ebdb))
  :config
  (defun cp-export-ebdb ()
    "Export my whole EBDB to a single VCF file, used for sync"
    (interactive)
    (ebdb-format-all-records ebdb-vcard-default-40-formatter)
    (with-current-buffer "*EBDB Format*"
      (write-region (point-min) (point-max) "~/vcards.vcf"))
    (let* ((buffer (get-buffer "*EBDB Format*"))
           (wins (get-buffer-window-list buffer nil t)))
      (kill-buffer buffer)
      (dolist (win wins)
        (when (window-live-p win)
          (delete-window win))))))

(use-package ebdb-complete
  :after (ebdb))
