(setq package-selected-packages (cons 'haml-mode package-selected-packages))
(use-package haml-mode
  :ensure t
  :mode "\\.haml\\'"
  :hook (haml-mode . cp-code-mode-hook))
