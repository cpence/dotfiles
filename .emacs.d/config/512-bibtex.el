(use-package bibtex
  :hook ((bibtex-mode . bu-parse-keywords-values)
         (bibtex-mode . bu-ensure-keywords-hook)
         (bibtex-mode . display-fill-column-indicator-mode)
         (bibtex-mode . bu-show-long-lines))
  :bind (("<f7>" . cp-open-library)
         :map bibtex-mode-map
         ("C-c k" . bu-make-field-keywords)
         ("C-c C-v" . bu-open-file) ;; parallel TeX mode
         ("C-c C-f" . bu-add-file)
         ("C-c f" . bibtex-make-field) ;; moved from C-c C-f
         ("C-c C-d" . bu-delete-files)
         ("C-c C-n" . bu-open-note)
         ("C-c C-t" . bu-toggle-tablet)

         ;; Originally, "C-c C-s" is "find entry by key" and "C-c C-a" is search
         ;; within entries by field. Move those binds around.
         ("C-c j" . bibtex-search-entry) ;; "jump" to entry by key
         ("C-c C-j" . bibtex-search-entry)
         ("C-c s" . bibtex-search-entries) ;; find entry by field
         ;; "C-c C-s" is below in consult-recoll

         ;; Never use InBook, for any reason, rebind it out of existence
         ("C-c C-e C-b" . (lambda ()
                            (interactive)
                            (bibtex-entry "InCollection")))
         ("C-c C-e I" . (lambda ()
                          (interactive)
                          (bibtex-entry "InCollection"))))
  :init
  (defun cp-open-library ()
    "Open my main library file."
    (interactive)
    (find-file "~/Nextcloud/Library/library.bib"))
  
  (defvar bu-find-dir "~/Downloads/")
  (defvar bu-tablet-dir "~/Nextcloud/Hold/iPad Papers/")
  (defvar bu-default-library-dir "~/Nextcloud/Library/")

  ;; Highlight any long lines
  (defun bu-show-long-lines ()
    (setq-local whitespace-style '(face lines))
    (setq-local whitespace-line-column 81)
    (whitespace-mode))
  
  ;; Nice keywords support
  (make-variable-buffer-local 'bu-keywords-values)
  (defun bu-parse-keywords-values ()
    (setq bu-keywords-values (bu-collect-keywords-values)))

  (defun bu-collect-keywords-values (&optional regexp)
    "Collect values in keywords fields of all BibTeX entries.
Maybe restrict the values to those matching REGEXP. Keywords may
be phrases separated by commas. Multiple spaces or newlines
within a keyword will be removed before collection."
    (save-excursion
      (goto-char (point-min))
      (let (keywords kstring)
        (while (re-search-forward "^\\s-*keywords.*{\\([^}]+\\)}" nil t)
	        (setq kstring (replace-regexp-in-string "[ \t\n]+" " " (match-string 1)))
	        (mapc
	         (lambda (v) 
	           (if regexp (if (string-match regexp v) 
			                      (add-to-list 'keywords v t))
	             (add-to-list 'keywords v t)))
           (split-string kstring ",[ \n]*\\|{\\|}" t)))
        keywords)))

  (defun bu-make-field-keywords (&optional arg)
    "Make a keywords field.
If ARG is nil, ask for each keyword and offer completion over
keywords that are already available in the buffer. Inserting the
empty string will quit the prompt. If the keyword is not already
present in the buffer, it will be added to the local variable
bu-keywords-values."
    (interactive "P")
    (let ((elist (save-excursion (bibtex-beginning-of-entry)
			                           (bibtex-parse-entry)))
	        append)
      (if (assoc "keywords" elist)
	        (progn (setq append t)
	               (bibtex-beginning-of-entry)
	               (goto-char 
		              (car (last (or (bibtex-search-forward-field "keywords" t)
                                 (progn (setq append nil)
                                        (bibtex-search-forward-field "OPTkeywords" t)))))))
        (bibtex-make-field "keywords" t nil))
      (skip-chars-backward "}")
      (unless arg
        (let ((cnt 0)
              k)
	        (while (and (setq k (completing-read 
                               "Keyword (RET to quit): " bu-keywords-values nil))
		                  (not (equal k "")))
	          (when append (insert ", ")
                  (setq append nil))
	          (setq cnt (1+ cnt))
	          (insert (format "%s%s" (if (> cnt 1) ", " "") k))
            (add-to-list 'bu-keywords-values k))))))

  ;; Document note support
  (defun bu-make-org-roam-note (title key)
    "Create a literature note for the given paper title and BibTeX key"
    (let* ((slug (org-roam-node-slug (org-roam-node-create :title title)))
           (filename
            (expand-file-name
             (org-roam-capture--fill-template "%<%Y%m%d%H%M%S>.org")
             org-roam-directory))
           id)
      (progn
        (with-temp-buffer
          (insert
           (format ":PROPERTIES:\n:ID:       \n:ROAM_REFS: [cite:@%s]\n:END:\n#+title: %s\n[cite:@%s]\n\n"
                   key title key))
          (write-file filename)
          (goto-char 25)
          (setq id (org-id-get-create))
          (org-roam-db-update-file filename))
        id)))

  (defun bu-get-last-name (fullname)
    "Get the last name from an author's name"
    ;; Make sure that we do not lowercase or abbreviate the name
    (let ((bibtex-autokey-name-case-convert-function 'identity)
          (bibtex-autokey-name-length 'infty))
      (bibtex-autokey-demangle-name fullname)))

  (defun bu-open-note ()
    "Create or open a note in the `annote' field for a given bibtex entry"
    (interactive)
    (let ((elist (save-excursion (bibtex-beginning-of-entry)
                                 (bibtex-parse-entry))))
      (if (assoc "annote" elist)
          (progn
            (bibtex-beginning-of-entry)
            (goto-char (cadr (bibtex-search-forward-field "annote" t)))
            (skip-chars-forward "{")
            (org-open-at-point-global))
        (progn
          (let* (;; Get the first author, title, and year of the entry
                 (authors (bibtex-text-in-field "author\\|editor"))
                 (fullname (car (split-string authors "[ \t\n]+and[ \t\n]+")))
                 (name (bu-get-last-name fullname))
                 (year (bibtex-text-in-field "year"))
                 (title (replace-regexp-in-string "[ \t\n]+" " " (bibtex-text-in-field "title")))
                 ;; Get the entry type and key
                 (entry-type (cdr (assoc "=type=" elist)))
                 (entry-key (cdr (assoc "=key=" elist)))
                 ;; Add quotation marks or not, depending on what kind of entry
                 ;; this is
                 (entry-title (if (or (bibtex-string= entry-type "book")
                                      (bibtex-string= entry-type "manual")
                                      (bibtex-string= entry-type "mastersthesis")
                                      (bibtex-string= entry-type "phdthesis")
                                      (bibtex-string= entry-type "proceedings")
                                      (bibtex-string= entry-type "techreport"))
                                  (format "%s (%s), %s" name year title)
                                (format "%s (%s), “%s”" name year title)))
                 ;; Make the node
                 (id (bu-make-org-roam-note entry-title entry-key)))
            ;; Save the annotation
            (bibtex-make-field "annote" t nil)
            (skip-chars-backward "}")
            (insert (format "id:%s" id))
            (save-buffer)
            ;; Open the new file in a proper buffer
            (org-open-at-point-global))))))
  
  ;; Document opening support
  (defun bu-library-dir ()
    "Get the library directory, AKA the folder in which you find this bib file"
    (if (buffer-file-name)
        (file-name-directory (buffer-file-name))
      (expand-file-name bu-default-library-dir)))
  
  (defun bu-file-list (key)
    "Get all the attachments for the given key.
This returns all documents named with the key, including optional
extra documents."
    (directory-files (concat (bu-library-dir) (substring key 0 1)) t
                     (concat key "\\(-[0-9a-z]+\\)?\\..+")))

  (defun bu-tablet-file-list (key)
    "Get all the attachments for the given key in the tablet dir."
    (directory-files bu-tablet-dir t (concat key "\\(-[0-9a-z]+\\)?\\..+")))
  
  (defun bu-open-file ()
    "Open the document for the current bibtex entry.
First tries to open any documents named after the entry key.
Failing that, it will check for a doi, and finally a url."
    (interactive)
    (save-excursion
      (bibtex-beginning-of-entry)
      (let* ((elist (bibtex-parse-entry))
             (key (cdr (assoc "=key=" elist)))
             (files (bu-file-list key))
             (file-name
              (cond ((= (length files) 1)
                     (car files))
                    ((> (length files) 1)
                     (completing-read "Choose file: " files))
                    (t nil)))
             (doi (assoc "doi" elist))
             (url (assoc "url" elist)))
        (cond ((and file-name)
               (message "Opening file externally...")
               (cp-open-file file-name))
              ((and doi)
               (message "Opening DOI in browser...")
               (cp-open-file (concat "https://doi.org/" (cdr doi))))
              ((and url)
               (message "Opening URL in browser...")
               (cp-open-file (cdr url)))
              (t (message "No attached files, and no DOI or URL present!"))))))

  (defun bu-add-file ()
    "Add a file to the entry at point.
Constructs the filename that we expect, named after the key of
the current entry, shows a find-file line, and moves that file to
the destination. Will abort if a file is already present."
    (interactive)
    (save-excursion
      (bibtex-beginning-of-entry)
      (let* ((key (cdr (assoc "=key=" (bibtex-parse-entry))))
             (files (bu-file-list key))
             (in-file-name
              (if files
                  ;;; FIXME: we should add key-1, -2, -3, etc. until we find one
                  ;;; that doesn't exist
                  (error "Files already exist for this entry, won't attach!")
                (read-file-name "File to attach: " bu-find-dir nil t)))
             (extension (file-name-extension in-file-name t))
             (out-file-name (concat (bu-library-dir)
                                    (substring key 0 1) "/"
                                    key extension)))
        (rename-file in-file-name out-file-name))))

  (defun bu-delete-files ()
    "Delete all files for this entry from the hard drive."
    (interactive)
    (save-excursion
      (bibtex-beginning-of-entry)
      (let* ((key (cdr (assoc "=key=" (bibtex-parse-entry))))
             (files (bu-file-list key)))
        (unless files
          (error "No files to delete, aborting!"))
        (if (y-or-n-p "Really delete all files that belong to this entry? ")
            (dolist (file files)
              (delete-file file))))))
  
  (defun bu-send-to-tablet ()
    "Send the files for the entry at point to the tablet dir."
    (interactive)
    (save-excursion
      (bibtex-beginning-of-entry)
      (let* ((key (cdr (assoc "=key=" (bibtex-parse-entry))))
             (files (bu-file-list key)))
        (unless files
          (error "No files to move, aborting!"))
        (if (y-or-n-p "Move files for this entry to the tablet? ")
            (dolist (file files)
              (rename-file file bu-tablet-dir))))))

  (defun bu-get-from-tablet ()
    "Get the files for the entry at point from the tablet dir."
    (interactive)
    (save-excursion
      (bibtex-beginning-of-entry)
      (let* ((key (cdr (assoc "=key=" (bibtex-parse-entry))))
             (files (bu-tablet-file-list key))
             (dir (concat (bu-library-dir)
                          (substring key 0 1) "/")))
        (unless files
          (error "No files for this entry on tablet, aborting!"))
        (if (y-or-n-p "Move files for this entry back to the library? ")
            (dolist (file files)
              (rename-file file dir))))))

  (defun bu-toggle-tablet ()
    "Send or get files for this entry from the tablet, as appropriate"
    (interactive)
    (save-excursion
      (bibtex-beginning-of-entry)
      (let* ((key (cdr (assoc "=key=" (bibtex-parse-entry))))
             (library-files (bu-file-list key))
             (tablet-files (bu-tablet-file-list key)))
        (cond ((and library-files tablet-files)
               (error "There are files both in library and on tablet!"))
              ((and library-files)
               (bu-send-to-tablet))
              ((and tablet-files)
               (bu-get-from-tablet))
              (t (message "No files for entry either in library or on tablet"))))))

  ;; Let me fiddle with this in file-local variables
  (defvar-local bu-check-keywords t)
  (add-to-list 'safe-local-variable-values '(bu-check-keywords))
  (add-to-list 'safe-local-variable-values '(eval bu-check-keywords t))
  (add-to-list 'safe-local-variable-values '(eval bu-check-keywords nil))

  ;; Check on every save that we have keywords for all entries
  (defun bu-ensure-keywords ()
    "Ensure that every entry in buffer has keywords available."
    (interactive)
    (if bu-check-keywords
        (bibtex-map-entries
         (lambda (key begin end)
           (if (not (assoc "keywords" (bibtex-parse-entry)))
               (message "WARNING: Entry `%s' does not have any keywords" key))))))

  (defun bu-ensure-keywords-hook ()
    (add-hook 'after-save-hook 'bu-ensure-keywords nil 'local))

  :custom
  ;; Style
  (bibtex-align-at-equal-sign t)
  (bibtex-field-delimiters 'braces)
  (bibtex-comma-after-last-field nil)
  (bibtex-entry-format
   '(opts-or-alts
     required-fields
     numerical-fields
     whitespace
     sort-fields))

  ;; Cite keys
  (bibtex-autokey-names 1)
  (bibtex-autokey-year-length 4)
  (bibtex-autokey-titlewords 1)
  (bibtex-autokey-titleword-ignore '("A" "An" "On" "The"
                                     "Eine?" "Der" "Die" "Das"
                                     "[Dd]e" "[Dd]es" "[Ll]e" "[Ll]a" "[Ll]es"
                                     "[^[:upper:]].*"
                                     ".*[^[:upper:][:lower:]0-9].*"))
  (bibtex-autokey-titlewords-stretch 0)
  (bibtex-autokey-name-year-separator "_")
  (bibtex-autokey-year-title-separator "_")
  
  ;; Optional fields
  (bibtex-user-optional-fields
   '(("annote" "Personal annotation (ignored)")
     ("keywords" "List of comma-separated keywords")
     ("doi" "DOI (without https://doi.org/)")
     ("url" "URL (do not duplicate DOI)")
     ("language" "Language (if not English)")))
  
  ;; Keep the buffer sorted so we can detect duplicate keys
  (bibtex-maintain-sorted-entries t))

;; Full-text library searching with consult-recoll
(use-package consult-recoll
  :ensure t
  :after (bibtex)
  :bind (:map bibtex-mode-map
              ("C-c C-s" . cp-recoll-library)) ;; search library fulltext
  :custom
  (consult-recoll-group-by-mime nil)
  (consult-recoll-inline-snippets t)
  ;; Open everything externally with my MIME handler
  (consult-recoll-open-fn 'cp-recoll-open-file)

  :init
  (defun cp-recoll-open-file (file page)
    (cp-open-file file))
  
  (defun cp-recoll-library ()
    (interactive)
    (let ((consult-recoll-search-flags
           `("-A" "-p" "5"
             ,(concat "dir:" (expand-file-name bu-default-library-dir)))))
      (consult-recoll nil))))
