(use-package emacs
  :bind (;; Don't let Emacs suspend the frame
         ("C-z" . nil)
         ("C-x C-z" . nil)

         ;; Put this on an easier key to find
         ("C-x C-k" . kill-buffer-and-window)))
