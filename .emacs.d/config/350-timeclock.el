(use-package timeclock
  :bind
  (("C-c t i" . cp-timeclock-in-or-change)
   ("C-c t o" . timeclock-out))
  :config
  (defun cp-timeclock-in-or-change ()
    "Clock into a project, clocking out first if we are already clocked in."
    (interactive)
    (when (equal (car timeclock-last-event) "i")
      (timeclock-out)
      (timeclock-reread-log))
    (call-interactively 'timeclock-in))
  
  :custom
  (timeclock-file (expand-file-name "~/Nextcloud/timelog"))
  (timeclock-get-reason-function nil))
