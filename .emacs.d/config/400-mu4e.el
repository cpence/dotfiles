(use-package mu4e
  :ensure nil
  :hook ((mu4e-compose-mode . cp-mu4e-contexts-setup-completion)
         (mu4e-search . cp-mu4e-setup-headers))
  :bind (("<f3>" . mu4e)
         ("<f4>" . cp-mu4e-news)
         :map mu4e-compose-mode-map
         ;; Complete EBDB contacts, not mu4e contacts
         ("<tab>" . cp-ebdb-complete))
  :custom
  ;; Basic configuration for Emacs mail as a whole
  (read-mail-command 'mu4e)
  (user-mail-address "charles@charlespence.net")
  (user-full-name "Charles Pence")

  ;; Mailbox configuration
  (mu4e-drafts-folder "/Drafts")
  (mu4e-trash-folder "/Trash")
  (mu4e-sent-folder "/Archive.2025")
  (mu4e-refile-folder "/Archive.2025")

  ;; Get mail, but not automatically
  (mu4e-get-mail-command "~/bin/mail-get")
  (mu4e-update-interval nil)
  (mu4e-modeline-show-global nil)

  ;; Only use normal Emacs completions
  (mu4e-read-option-use-builtin nil)
  (mu4e-completing-read-function 'completing-read)

  ;; Disable mu4e's address completion system, we don't need it
  (mu4e-compose-complete-addresses nil)

  ;; Don't ask about viewing contexts (unified inbox), but do ask about compose
  ;; contexts if you can't figure them out via the match rules
  (mu4e-context-policy nil)
  (mu4e-compose-context-policy 'ask-if-none)

  ;; Jump shortcuts for mailboxes and searches
  (mu4e-maildir-shortcuts
   '((:maildir "/Inbox" :key ?i)
     (:maildir "/Action" :key ?a)
     (:maildir "/Hold" :key ?h)
     (:maildir "/Drafts" :key ?d)
     (:maildir "/Trash" :key ?t)))

  (mu4e-bookmarks
   '((:name "Unread messages" :query "flag:unread AND NOT maildir:/News AND NOT flag:trashed" :key ?u)
     (:name "Today" :query "date:today..now AND NOT maildir:/News AND NOT flag:trashed" :key ?t)
     (:name "Yesterday" :query "date:2d..now AND NOT date:today..now AND NOT maildir:/News AND NOT flag:trashed" :key ?y)
     (:name "This week" :query "date:7d..now AND NOT maildir:/News AND NOT flag:trashed" :key ?w)
     (:name "Year 2025" :query "date:20250101..20251231 AND NOT maildir:/News AND NOT flag:trashed" :key ?5)
     (:name "Year 2024" :query "date:20240101..20241231 AND NOT maildir:/News AND NOT flag:trashed" :key ?4)
     (:name "Sent messages" :query "(from:charles@charlespence.net OR from:charles.pence@uclouvain.be OR from:julia.and@charlespence.net) AND NOT flag:trashed" :key ?s)))

  ;; Configure the home screen
  (mu4e-main-hide-personal-addresses t)

  ;; Don't show encrypted, signed, list, or personal by default
  (mu4e-headers-visible-flags
   '(draft flagged new replied passed trashed attach))
  (mu4e-use-fancy-chars t)

  ;; Don't show full threads by default
  (mu4e-search-include-related nil)

  ;; Save attachments to download folder
  (mu4e-attachment-dir "~/Downloads")

  :init
  (defun cp-mu4e-news ()
    "Show my news feeds, with custom sorting and fields"
    (interactive)
    ;; Start mu4e if it's not yet running, otherwise these functions won't be
    ;; bound
    (unless (and (boundp 'mu4e-running-p) (mu4e-running-p))
      (mu4e t))
    ;; Reset threading and search and start mu4e (binds will reset when we leave
    ;; this function)
    (let ((mu4e-search-threads nil)
          (mu4e-headers-sort-direction 'ascending)
          (mu4e-headers-sort-field :tags))
      (mu4e-search "m:/News")))

  :config
  ;; This doesn't seem to work unless we do it here
  (setq mail-user-agent #'mu4e-user-agent)
  
  ;; Multiple sending contexts
  ;;
  ;; This needs to be set in :config because it uses the function
  ;; make-mu4e-context to build the relevant variable
  (setq mu4e-contexts
        `(,(make-mu4e-context
            :name "Personal"
            :enter-func (lambda () (mu4e-message "Entering personal context"))
            :leave-func (lambda () (mu4e-message "Leaving personal context"))
            :match-func (lambda (msg)
                          (when msg
                            (mu4e-message-contact-field-matches
                             msg
                             '(:to :cc) "charles@charlespence.net")))
            :vars '((user-mail-address . "charles@charlespence.net")))
          ,(make-mu4e-context
            :name "Work"
            :enter-func (lambda () (mu4e-message "Entering work context"))
            :leave-func (lambda () (mu4e-message "Leaving work context"))
            :match-func (lambda (msg)
                          (when msg
                            (mu4e-message-contact-field-matches
                             msg
                             '(:to :cc) "charles.pence@uclouvain.be")))
            :vars '((user-mail-address . "charles.pence@uclouvain.be")))))

  ;; Customize some marks (for some reason, this doesn't work in :custom)
  (setq mu4e-headers-draft-mark   '("D" . "󰏪")
        mu4e-headers-flagged-mark '("*" . "󰄬")
        mu4e-headers-replied-mark '("R" . "󰑚")
        mu4e-headers-passed-mark  '("F" . "󰊍")
        mu4e-headers-trashed-mark '("T" . "󰩺")
        mu4e-headers-attach-mark  '("a" . "󰏢"))

  ;; Change header line when I'm in a news folder
  (defun cp-mu4e-setup-headers (search)
    (if (string-match-p "m:/News" search)
        (progn
          ;; This is the news folder, so add tags and remove date
          (setq mu4e-headers-fields '((:tags . 32)
                                      (:from-or-to . 25)
                                      (:subject)))
          ;; Force a header-line update
          (setq header-line-format (mu4e~header-line-format)))
      (progn
        ;; This is a normal folder, so remove tags
        (setq mu4e-headers-fields '((:human-date . 12)
                                    (:flags . 6)
                                    (:from-or-to . 25)
                                    (:subject)))
        ;; Force a header-line update
        (setq header-line-format (mu4e~header-line-format)))))        

  ;; When students e-mail me assignments, I want to save them with a defined
  ;; filename based on their e-mail address, so that I can return them using
  ;; console scripts.
  ;;
  ;; Because this is for student work, we only keep a certain list of file
  ;; extensions.
  ;;
  ;; This is largely based on mu4e-view-save-attachments.
  (defvar cp-student-work-dir "~/Nextcloud/Work/Teaching/Incoming"
    "The directory in which I want to save incoming student work")
  (defvar cp-student-work-exts '(".pdf" ".doc" ".docx" ".odt")
    "The file extensions that are automatically captured as student work")

  (defun cp-fname-compare (str suffix)
    "Returns t if str ends with suffix"
    (string-suffix-p suffix str t))

  (defun cp-student-work-filename (msg fname)
    "Build a filename based on the student's email address"
    (let* ((extension (file-name-extension fname t))
           (from (car (mu4e-message-field (mu4e-message-at-point) :from)))
           (mailaddr (mu4e-contact-email from)))
      (concat mailaddr extension)))

  (defun cp-save-student-work (msg)
    "Save all student work attached to the given message to the incoming folder"
    (interactive)
    (cl-assert (and (eq major-mode 'mu4e-view-mode)
                    (derived-mode-p 'gnus-article-mode)))

    ;; This section is largely borrowed from mu4e-view-save-attachments
    (let* ((parts (mu4e-view-mime-parts))
           (candidates (seq-map
                        (lambda (fpart)
                          (cons ;; (filename . annotation)
                           (plist-get fpart :filename)
                           fpart))
                        (seq-filter
                         (lambda (part) (plist-get part :attachment-like))
                         parts)))
           (result nil))
      (dolist (cand candidates)
        (let ((fname (car cand))
              (part (cdr cand)))
          (when (and fname (cl-member fname cp-student-work-exts
                                      :test #'cp-fname-compare))
            ;; This is a good one, save it
            (let ((path (mu4e--uniquify-file-name
                         (expand-file-name
                          (cp-student-work-filename msg fname)
                          cp-student-work-dir))))
              (mm-save-part-to-file (plist-get part :handle) path)
              (setq result t))))
        (unless result
          (mu4e-message "No attached work-like files found")))))

  (add-to-list 'mu4e-view-actions
               '("wsave student work" . cp-save-student-work) t)

  ;; Make sure EBDB is loaded and complete a mail address with it. This is taken
  ;; from the code in ebdb-notmuch.el and ebdb-complete.el.
  (defun cp-ebdb-complete ()
    (interactive)

    (cond
     ;; Type TAB launch ebdb-complete when in header.
     ((and (save-excursion
             (let ((point (point)))
               (message-goto-body)
               (> (point) point)))
           (not (looking-back "^\\(Subject\\|From\\): *.*"
                              (line-beginning-position)))
           (not (looking-back "^" (line-beginning-position))))
      (unless (bound-and-true-p ebdb-db-list)
        (ebdb-load))
      (ebdb-complete-mail))
     (message-tab-body-function (funcall message-tab-body-function))
     (t (funcall (or (lookup-key text-mode-map "\t")
                     (lookup-key global-map "\t")
                     'indent-relative)))))

  ;; Build a simple completion function for completing composition contexts.
  ;;
  ;; I want to be able to easily choose between the From addresses in my
  ;; different composition contexts. This is a reasonably simple
  ;; completion-at-point function based on those found in mu4e itself.
  (defun cp-mu4e-context-completion-list ()
    "Return a list of all of our contexts for completion after From"
    (cl-loop for context in mu4e-contexts
             collect (with-mu4e-context-vars context
                         (concat user-full-name " <" user-mail-address ">"))))

  (defun cp-mu4e-complete-context (&optional start)
    "The completion-at-point-function for completing contexts after From"
    (interactive)
    (let ((from-header
           (save-excursion
             (beginning-of-line)
             ;; skip backwards over continuation lines.
             (while (and (looking-at "^[ \t]")
		                     (not (= (point) (point-min))))
	             (forward-line -1))
             ;; are we at the front of an appropriate header line?
             (looking-at "^\\(From\\):")))
          (eoh ;; end-of-headers
           (save-excursion
             (goto-char (point-min))
             (search-forward-regexp mail-header-separator nil t))))
      ;; Complete contexts only when we're in the headers area, after From
      (when (and eoh (> eoh (point)) from-header)
        (let* ((end (point))
               (start
                (or start
                    (save-excursion
                      (re-search-backward "\\(\\`\\|[\n:,]\\)[ \t]*")
                      (goto-char (match-end 0))
                      (point)))))
          (list start end (cp-mu4e-context-completion-list))))))

  (defun cp-mu4e-contexts-setup-completion ()
    "Set up my custom auto-completion of composition contexts."
    (add-hook 'completion-at-point-functions
              'cp-mu4e-complete-context -50 t)))
