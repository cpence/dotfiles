(setq package-selected-packages (cons 'citar package-selected-packages))
(use-package citar
  :ensure t
  :bind (("C-c b" . citar-insert-citation)
         :map minibuffer-local-map
         ("M-b" . citar-insert-preset))
  :custom
  ;; Basic setup for the library and finding files
  (citar-bibliography org-cite-global-bibliography)
  (citar-library-file-extensions (list "pdf" "jpg"))
  (citar-file-additional-files-separator "-")

  ;; Not using citar notes
  (citar-open-resources '(:files :links))

  ;; This is a hack, but it should work to make citar see my files
  (citar-library-paths '("~/Nextcloud/Library/a" "~/Nextcloud/Library/n"
                         "~/Nextcloud/Library/b" "~/Nextcloud/Library/o"
                         "~/Nextcloud/Library/c" "~/Nextcloud/Library/p"
                         "~/Nextcloud/Library/d" "~/Nextcloud/Library/q"
                         "~/Nextcloud/Library/e" "~/Nextcloud/Library/r"
                         "~/Nextcloud/Library/f" "~/Nextcloud/Library/s"
                         "~/Nextcloud/Library/g" "~/Nextcloud/Library/t"
                         "~/Nextcloud/Library/h" "~/Nextcloud/Library/u"
                         "~/Nextcloud/Library/i" "~/Nextcloud/Library/v"
                         "~/Nextcloud/Library/j" "~/Nextcloud/Library/w"
                         "~/Nextcloud/Library/k" "~/Nextcloud/Library/x"
                         "~/Nextcloud/Library/l" "~/Nextcloud/Library/y"
                         "~/Nextcloud/Library/m" "~/Nextcloud/Library/z"))
  
  ;; Full connection between citar and Org
  (org-cite-insert-processor 'citar)
  (org-cite-follow-processor 'citar)
  (org-cite-activate-processor 'citar)

  ;; Show the bib entry when a cite: link is followed
  (citar-at-point-function #'cp-citar-open-entry-at-point)

  :config
  (defun cp-citar-open-entry-at-point ()
    "Open the bib entry for the cite key at point."
    (interactive)
    (if-let ((citekeys (or (citar-key-at-point) (citar-citation-at-point))))
        (funcall citar-open-entry-function 
                 (if (listp citekeys) (car citekeys) citekeys))
      (user-error "No citation keys found"))))
