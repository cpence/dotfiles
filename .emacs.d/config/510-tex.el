(setq package-selected-packages (cons 'auctex package-selected-packages))
(use-package tex
  :ensure auctex
  ;; Load every TeX file in latex-mode, don't try to auto-detect
  :mode ("\\.[Tt]e[Xx]\\'" . latex-mode)
  :hook (TeX-mode . cp-tex-mode-hook)
  :custom
  ;; Basic functions for intelligent parsing of files
  (TeX-auto-save t)
  (TeX-parse-self t)
  (TeX-style-private (expand-file-name "~/.emacs.d/auctex-styles"))

  ;; Use latexmk to build and view
  (TeX-command-list
   '(("Latexmk" "latexmk %s" TeX-run-command t t :help "Build with latexmk")
     ("View" "latexmk -pv %s" TeX-run-command t t :help "View file output")
     ("Clean" "latexmk -C %s" TeX-run-command t t :help "Clean intermediate files")))
  (TeX-view-program-list
   '(("Latexmk" ("latexmk -pv %s"))))
  (TeX-view-program-selection
   '((t "Latexmk")))
  (TeX-save-query nil)

  :init
  ;; Let me fiddle with these in file-local variables
  (add-to-list 'safe-local-variable-values '(TeX-auto-save))
  (add-to-list 'safe-local-variable-values '(eval TeX-auto-save t))
  (add-to-list 'safe-local-variable-values '(eval TeX-auto-save nil))
  (add-to-list 'safe-local-variable-values '(TeX-parse-self))
  (add-to-list 'safe-local-variable-values '(eval TeX-parse-self t))
  (add-to-list 'safe-local-variable-values '(eval TeX-parse-self nil))

  :config
  (defun cp-tex-mode-hook ()
    "A hook that will configure TeX as a mix between text and code modes"
    (wc-mode)
    (auto-fill-mode)
    (jinx-mode)

    ;; Always build with latexmk
    (setq TeX-command-default "Latexmk")))

