(setq package-selected-packages (cons 'lua-mode package-selected-packages))
(use-package lua-mode
  :ensure t
  :mode "\\.lua\'"
  ;; There is a lua-lsp package in extreme alpha; not using it
  :hook (lua-mode . cp-code-mode-hook))
