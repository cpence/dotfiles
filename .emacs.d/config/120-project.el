(use-package project
  :custom
  ;; I don't actually garden this, because I use treemacs' ability to
  ;; auto-select projects based on vc-dir
  (project-list-file "~/.cache/emacs/projects"))
