(setq package-selected-packages (cons 'jinx package-selected-packages))
(use-package jinx
  :ensure t
  :delight (jinx-mode "  󰓆")
  :bind (("<f8>" . jinx-mode)
         ("C-<f8>" . jinx-correct))
  :init
  (setq-default jinx-languages "en_US fr_BE"))
