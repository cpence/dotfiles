(use-package midnight
  :custom
  ;; Clean my unused buffers every two hours
  (midnight-timer 7200)
  ;; If I haven't visted a buffer in a day, kill it
  (clean-buffer-list-delay-general 1)
  :init
  (midnight-mode))
