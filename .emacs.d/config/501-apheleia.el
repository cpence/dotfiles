(setq package-selected-packages (cons 'apheleia package-selected-packages))
(use-package apheleia
  :ensure t
  :custom
  (apheleia-formatters-respect-fill-column t)
  ;; This package doesn't work well with delight
  (apheleia-mode-lighter nil)
  :config
  (apheleia-global-mode +1)

  ;; Pick syntax-tree for Ruby formatting
  (setf (alist-get 'ruby-ts-mode apheleia-mode-alist) 'ruby-syntax-tree)
  
  ;; Add HAML formatting with syntax-tree
  (setf (alist-get 'haml-mode apheleia-mode-alist) 'ruby-syntax-tree)

  ;; Add yq formatting of YAML, CSV, TSV, and JSON files
  (setf (alist-get 'json-ts-mode apheleia-mode-alist) 'yq-json)
  (setf (alist-get 'yaml-ts-mode apheleia-mode-alist) 'yq-yaml)
  ;; ... yq-csv
  ;; ... yq-tsv

  ;; Add shell-script formatting
  (setf (alist-get 'shell-script-mode apheleia-mode-alist) 'shfmt)

  ;; Enable both black and isort for Python
  (setf (alist-get 'python-ts-mode apheleia-mode-alist) '(isort black))

  ;; Do not auto-format TeX with latexindent
  (assq-delete-all 'latex-mode apheleia-mode-alist)
  (assq-delete-all 'LaTeX-mode apheleia-mode-alist)
  (assq-delete-all 'TeX-latex-mode apheleia-mode-alist)
  (assq-delete-all 'TeX-mode apheleia-mode-alist)
  (assq-delete-all 'latexindent apheleia-formatters))
