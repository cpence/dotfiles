(setq package-selected-packages (cons 'alert package-selected-packages))
(use-package alert
  :ensure t
  :custom
  (alert-default-style 'notifications))
