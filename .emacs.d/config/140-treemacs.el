(setq package-selected-packages (cons 'treemacs package-selected-packages))
(use-package treemacs
  :ensure t
  :bind ("<f12>" . cp-open-treemacs)
  :custom
  (treemacs-indent-guide-style 'line)
  :config
  (treemacs-follow-mode t)
  (treemacs-project-follow-mode t)
  (treemacs-fringe-indicator-mode 'always)
  (treemacs-git-mode 'deferred)
  (treemacs-filewatch-mode t)
  (treemacs-indent-guide-mode t)
  :init
  (defun cp-open-treemacs ()
    "Open treemacs, and grow or shrink the current frame to compensate"
    (interactive)
    (let* ((tree-width
            (+ (if (boundp 'treemacs-width)
                   treemacs-width
                 35)
               1))
           (new-frame-width
            (if (and (fboundp 'treemacs-current-visibility)
                     (eq (treemacs-current-visibility) 'visible))
                (- (frame-width) tree-width)
              (+ (frame-width) tree-width))))
      (set-frame-width (selected-frame) new-frame-width)
      (treemacs))))

(setq package-selected-packages (cons 'treemacs-all-the-icons package-selected-packages))
(use-package treemacs-all-the-icons
  :ensure t
  :config
  (treemacs-load-theme "all-the-icons"))
