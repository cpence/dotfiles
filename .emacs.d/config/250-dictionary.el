;; Custom code for defining words using a set of offline dictionaries, including
;; translations
;;
;; This is borrowed, and made multilingual, from
;; https://github.com/abo-abo/define-word
(defvar define-word-offline-dict-directory "~/Nextcloud/Dictionaries/Wiktionary"
  "Path to the directory which contains dictionaries downloaded from Wiktionary.")

(defun define-word--to-string (word)
  (let* ((regex (concat "^" (downcase word) " "))
         (glob (expand-file-name "*.txt" define-word-offline-dict-directory))
         (dictionaries (file-expand-wildcards glob))
         (choices (mapcar #'file-name-sans-extension (mapcar #'file-name-nondirectory dictionaries)))
         (choice (car (completing-read-multiple "Dictionary: " choices)))
         (filename (expand-file-name (concat choice ".txt") define-word-offline-dict-directory))
         (res (shell-command-to-string
               (concat "rg --no-filename --color never --max-count=5 '" regex "' " filename))))
    (unless (= 0 (length res))
      res)))

(defun define-word (word)
  "Define WORD using offline dictionaries.

Defaults to using the (lowercased) word at point, but can be
edited by the user."
  (interactive (list (read-string "Word: " (downcase (thing-at-point 'word)))))
  (let* ((results (define-word--to-string word)))
    (message (if results
                 results
               "0 definitions found"))))

(use-package emacs
  :bind
  (("<f10>" . define-word)))
