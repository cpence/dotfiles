(setq package-selected-packages (cons 'web-mode package-selected-packages))
(use-package web-mode
  :ensure t
  :mode (;; Straight HTML and friends
         ("\\.html?\\'" . web-mode)
         ("\\.css\\'" . web-mode)
         ("\\.scss\\'" . web-mode)
         ("\\.sass\\'" . web-mode)
         ("\\.xml\\'" . web-mode)

         ;; Template engines
         ("\\.phtml\\'" . web-mode)
         ("\\.tpl\\.php\\'" . web-mode)
         ("\\.[agj]sp\\'" . web-mode)
         ("\\.as[cp]x\\'" . web-mode)
         ("\\.erb\\'" . web-mode)
         ("\\.mustache\\'" . web-mode)
         ("\\.djhtml\\'" . web-mode))
  :hook (web-mode . cp-code-mode-hook)
  :custom
  ;; Indentation
  (web-mode-markup-indent-offset 2)
  (web-mode-css-indent-offset 2)
  (web-mode-code-indent-offset 2))
