(setq package-selected-packages (cons 'fish-mode package-selected-packages))
(use-package fish-mode
  :ensure t
  :mode
  "\\.fish\\'"
  "/fish_funced\\..*\\'"
  :hook (fish-mode . cp-code-mode-hook))
