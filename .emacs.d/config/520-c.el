(use-package cc-mode
  :mode (("\\.c\\'" . c-ts-mode)
         ("\\.h\\'" . c-or-c++-ts-mode)
         ("\\.\\(CC?\\|HH?\\)\\'" . c++-ts-mode)
         ("\\.[ch]\\(pp\\|xx\\|\\+\\+\\)\\'" . c++-ts-mode)
         ("\\.\\(cc\\|hh\\)\\'" . c++-ts-mode)
         ("\\.i\\'" . c-ts-mode)
         ("\\.ii\\'" . c++-ts-mode)
         ("\\.lex\\'" . c-ts-mode)
         ("\\.y\\(acc\\)?\\'" . c-ts-mode))
  :hook (;; `pacin tree-sitter-c-git && pacin tree-sitter-cpp-git`
         (c-ts-mode . cp-code-mode-hook)
         (c++-ts-mode . cp-code-mode-hook)
         ;; clangd is installed by default with clang
         (c-ts-mode . eglot-ensure)
         (c++-ts-mode . eglot-ensure)))
