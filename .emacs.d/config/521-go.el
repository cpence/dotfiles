(use-package go-ts-mode
  :mode (("\\.go\\'" . go-ts-mode)
         ("go\\.mod\\'" . go-mod-ts-mode))
  :hook (;; `pacin tree-sitter-go-git && pacin tree-sitter-gomod-git`
         (go-ts-mode . cp-code-mode-hook)
         ;; gopls is installed by default with golang
         (go-ts-mode . eglot-ensure)))
