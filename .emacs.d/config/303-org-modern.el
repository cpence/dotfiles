(setq package-selected-packages (cons 'org-modern package-selected-packages))
(use-package org-modern
  :ensure t
  :hook ((org-mode . org-modern-mode)
         (org-agenda-finalize . org-modern-agenda))
  :custom
  (org-modern-star 'replace)

  ;; Set manually to match modus-operandi-tinted theme
  (org-modern-todo-faces
   `(("TODO" :background "#bfc9ff")
     ("NEXT" :background "#ff8f88")
     ("HOLD" :background "#f3d000")
     ("DONE" :background "#8adf80")))

  (org-modern-tag-faces
   `(("warn" :background "#ff8f88")
     ("Q1@1" :background "#a4d5f9")
     ("Q1@2" :background "#a4d5f9")
     ("Q2@1" :background "#a4d5f9")
     ("Q2@2" :background "#a4d5f9")))

  :init
  (defun cp-org-modern-variable-fix (&rest args)
    ;; Reset the org-modern table spacing width, which breaks in
    ;; variable-pitch-mode
    (setf org-modern--table-sp-width
          (window-font-width (selected-window) 'org-table)))
  (advice-add 'org-modern--pre-redisplay :after #'cp-org-modern-variable-fix))

(setq package-selected-packages (cons 'org-appear package-selected-packages))
(use-package org-appear
  :ensure t
  :hook (org-mode . org-appear-mode))
