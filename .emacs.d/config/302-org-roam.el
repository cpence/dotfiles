(setq package-selected-packages (cons 'org-roam package-selected-packages))
(use-package org-roam
  :ensure t
  :hook ((org-mode . cp-org-roam-hook))
  :custom
  (org-roam-database-connector 'sqlite-builtin)
  (org-roam-directory (concat org-directory "/Notes"))
  (org-roam-db-location "~/.cache/emacs/org-roam.db")
  (org-roam-mode-sections
   '((org-roam-backlinks-section :unique t)
     org-roam-reflinks-section))
  :bind (("<f6>" . cp-open-org-roam)
         ("S-<f6>" . cp-open-roam-research)
         ("C-c n f" . org-roam-node-find)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n t" . org-roam-tag-add)
         ("C-c n r" . cp-org-roam-rename-note)
         :map org-mode-map
         ;; Steal this bind from treemacs while in org notes
         ("<f12>" . cp-org-roam-buffer))
  :config
  (defun cp-open-org-roam ()
    (interactive)
    ;; UUID of my Nexus note
    (find-file "~/Nextcloud/Org/Notes/e7722799-8607-43b4-a960-eed056ca1e78.org")
    (cp-org-roam-buffer))

  (defun cp-open-roam-research ()
    (interactive)
    ;; UUID of my Research Notebook note
    (find-file "~/Nextcloud/Org/Notes/093fa356-7b5d-4cac-8a71-930ffccf1421.org")
    (cp-org-roam-buffer))

  (org-roam-db-autosync-mode)
  
  ;; Don't make org-roam nodes for headers with IDs because they have
  ;; attachments
  (setq org-roam-db-node-include-function
        (lambda ()
          (not (member "ATTACH" (org-get-tags)))))

  (setq org-roam-capture-templates '(("d" "default" plain "%?"
                                      :target (file+head "%<%Y%m%d%H%M%S>.org"
                                                         "#+TITLE: ${title}\n")
                                      :unnarrowed t)))

  (setq org-roam-node-display-template (concat "${title:*} "
                                               (propertize "${tags:10}" 'face 'org-tag)))

  ;; Set up the org-roam buffer with some reasonable defaults
  (add-to-list 'display-buffer-alist
               '("\\*org-roam\\*"
                 (display-buffer-in-direction)
                 (direction . right)
                 (window-width . 32)
                 (window-height . fit-window-to-buffer)))

  ;; Take a smaller subset around backlinks in the org-roam buffer
  (defun cp-org-roam-preview-fetcher ()
    (let* ((elem (org-element-context))
           (parent (org-element-property :parent elem)))
      ;; TODO: alt handling for non-paragraph elements
      (string-trim-right (buffer-substring-no-properties
                          (org-element-property :begin parent)
                          (org-element-property :end parent)))))

  (setq org-roam-preview-function #'cp-org-roam-preview-fetcher)

  ;; Don't offer org-mode tags for org-roam files; I use a separate tagset for
  ;; roam notes
  (defun cp-org-roam-tag-completions (orig-fun &rest args)
    (let ((old-org-tags org-tag-alist)
          (org-tag-alist nil)
          (ret (apply orig-fun args)))
      (setq org-tag-alizt old-org-tags)
      ret))
  (advice-add 'org-roam-tag-completions :around #'cp-org-roam-tag-completions)
  
  (defun cp-org-roam-buffer ()
    "Show the org-roam-buffer but grow the window to make it fit."
    (unless (eq (org-roam-buffer--visibility) 'visible)
      (if (< (frame-width) 140)
          (set-frame-width (selected-frame) 140))
      (org-roam-buffer-toggle)))

  (defun cp-org-roam-hook ()
    "If we're opening a Roam notes file, go to variable-pitch
 mode."
    (let ((path (buffer-file-name (buffer-base-buffer))))
      (if (and path (org-roam-descendant-of-p path org-roam-directory))
          (progn
            (cp-org-roam-buffer)
            (org-indent-mode 1)
            (cp-variable-pitch-mode))
        ;; Not in a roam note, so enable auto-fill-mode
        (auto-fill-mode 1))))

  (defun cp-org-roam-rename-note ()
    "Rename an org-roam note on disk to match its ID"
    (interactive)
    (let* ((node (org-roam-node-at-point))
           (id (org-roam-node-id node))
           (file (buffer-file-name (buffer-base-buffer)))
           (file-name (file-name-nondirectory file))
           (new-file (format "%s/%s.org" org-roam-directory id)))
      (unless (string-match-p file-name (format "%s.org" id))
        (save-buffer)
        (rename-file file new-file)
        (set-visited-file-name new-file t t)
        (org-roam-db-update-file new-file)
        (org-roam-message "File renamed to %S" (abbreviate-file-name new-file))))))

(setq package-selected-packages (cons 'consult-org-roam package-selected-packages))
(use-package consult-org-roam
  :ensure t
  :after org-roam
  :bind (("C-c n s" . consult-org-roam-search)
         ("C-c n b" . consult-org-roam-backlinks)
         ("C-c n l" . consult-org-roam-forward-links))
  :delight consult-org-roam-mode
  :init
  (consult-org-roam-mode)
  :custom
  (consult-org-roam-grep-func #'consult-ripgrep))

