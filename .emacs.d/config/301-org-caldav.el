(setq package-selected-packages (cons 'org-caldav package-selected-packages))
(use-package org-caldav
  :ensure t
  :custom
  ;; org-caldav uses this for cookie storage
  (url-cookie-file "~/.cache/emacs/cookies")

  ;; Shared calendar connection info (password in authinfo.gpg)
  (org-caldav-url "https://gyarados.swab.lu/remote.php/dav/calendars/cpence/")
  (org-caldav-calendar-id "home")

  ;; Where to put the results and sync state
  (org-caldav-save-directory (concat org-directory "/Agenda/"))
  (org-caldav-inbox (concat org-directory "/Agenda/shared.org"))
  (org-caldav-files nil)

  ;; Save backups in cache
  (org-caldav-backup-file (expand-file-name "~/.cache/emacs/org-caldav-backup.org")))
