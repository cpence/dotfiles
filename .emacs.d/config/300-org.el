;; Make sure we install updated org from the repositories
(assq-delete-all 'org package--builtins)
(assq-delete-all 'org package--builtin-versions)
(setq package-selected-packages (cons 'org package-selected-packages))

(use-package org
  :ensure t
  :bind (("C-c l" . org-store-link)
         ("C-c r" . cp-org-show-review)
         ("C-c c" . org-capture)
         ("<f2>" . cp-org-show-agenda)
         ("S-<f2>" . cp-org-quick-open)
         ("C-<f2>" . cp-org-show-notepad))
  :hook ((org-mode . jinx-mode)
         ;; auto-fill-mode is set by a hook in the org-roam config
         (org-mode . typo-mode)
         (org-mode . wc-mode)
         (org-agenda-finalize . cp-org-color-categories))
  :init
  (delight 'org-capture-mode nil 'org-capture)
  (delight 'org-indent-mode nil 'org-indent)
  :custom-face
  (org-indent ((t (:inherit (fixed-pitch org-hide)))))
  (org-table ((t (:inherit 'fixed-pitch))))
  :custom
  ;; Tell Org where to find all my stuff
  (org-directory "~/Nextcloud/Org")

  (org-default-notes-file (concat org-directory "/notepad.org"))
  (org-agenda-files (list (concat org-directory "/Agenda/")))

  (org-id-locations-file "~/.cache/emacs/org-id-locations")

  (org-attach-id-dir (concat org-directory "/Attachments"))
  (org-attach-store-link-p 'attached)

  ;; Refiling
  (org-refile-use-outline-path 'file)
  (org-refile-targets
   `((nil . (:level . 1))
     (org-agenda-files . (:maxlevel . 2))
     (,(concat org-directory "/Agenda/students.org") .
      (:regexp . "\\(?:\\(?:Note\\|Task\\|Event\\)s\\)"))))
  (org-reverse-note-order t)
  (org-outline-path-complete-in-steps nil)

  ;; I don't use these, so don't pollute my bookmarks with them
  (org-capture-bookmark nil)
  (org-bookmark-names-plist nil)

  ;; Archiving
  (org-archive-location (concat org-directory
                                "/Archive/archive-"
                                (format-time-string "%Y")
                                ".org::* From %s"))

  ;; Don't put in extra logging lines
  (org-log-repeat nil)

  ;; Enable citation support in notes
  (org-cite-global-bibliography
   '("~/Nextcloud/Library/library.bib"
     "~/Nextcloud/Library/Pearson Papers/pearson.bib"))
  ;; More configuration is in the citar and bibtex package configs

  ;; Allow me to use shift-selection without messing with me
  (org-support-shift-select t)

  ;; Don't put in any extra blank lines if you can help it
  (org-blank-before-new-entry '((heading . nil) (plain-list-item . nil)))

  ;; Configure TODO keywords and stuck projects
  (org-todo-keywords '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
                       (sequence "HOLD(h)" "|" "CANCELED(c@)")))
  (org-treat-S-cursor-todo-selection-as-state-change nil)
  (org-stuck-projects
   '("+LEVEL=1+project-alpha-beta-ignore" ("NEXT" "HOLD") nil ""))

  ;; Agenda view configuration
  (org-agenda-window-setup 'current-window)

  (calendar-week-start-day 1)
  (org-agenda-start-on-weekday nil)
  (org-agenda-start-day "+0d")
  (org-agenda-span 7)

  ;; Set timezone for icalendar exports
  (org-icalendar-timezone "Europe/Brussels")

  ;; I don't use the diary, so don't include it
  (org-agenda-include-diary nil)

  ;; I use the holiday calendar via org-calendar-holiday, so configure that
  (holiday-bahai-holidays nil)
  (holiday-hebrew-holidays nil)
  (holiday-islamic-holidays nil)
  (holiday-oriental-holidays nil)

  ;; Set Belgian holidays -- New Year's, Easter, and Christmas are already set
  ;; by the "general" (= US) holiday calendar
  (holiday-local-holidays
   '((holiday-fixed 5 1 "Fête du Travail")
     (holiday-easter-etc 39 "L'Ascension")
     (holiday-easter-etc 49 "La Pentecôte")
     (holiday-easter-etc 50 "Le lundi de la Pentecôte")
     (holiday-fixed 7 21 "Fête Nationale")
     (holiday-fixed 8 15 "L'Assomption")
     (holiday-fixed 11 1 "Toussaint")
     (holiday-fixed 11 11 "Armistice de 1918")))

  (org-agenda-tags-column 0)
  (org-agenda-remove-tags t)

  (org-agenda-block-separator ?─)
  (org-agenda-breadcrumbs-separator "  ")

  (org-agenda-time-grid '((daily today require-timed remove-match)
                          (800 1000 1200 1400 1600 1800 2000)
                          " ┄┄┄┄┄ " "┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄"))
  (org-agenda-current-time-string
   "⭠ now ──────────────────────────────────────────────────────────")

  (org-agenda-prefix-format '((agenda . " %-12:c%?-12t% s")
                              (todo . " %-12:c %-20b")
                              (tags . " %-12:c")
                              (search . " %-12:c")))
  
  ;; My main custom agenda view
  (org-agenda-custom-commands
   '(("c" "Primary agenda view"
      ((agenda ""
               ((org-deadline-warning-days 0)))
       (todo "NEXT"
             ((org-agenda-skip-function
               '(org-agenda-skip-entry-if 'deadline))
              (org-agenda-prefix-format "  %c  %? b")
              (org-agenda-overriding-header "Next Actions")))
       (alltodo ""
                ((org-agenda-skip-function
                  '(org-agenda-skip-entry-if 'notdeadline 'todo '("HOLD" "DONE")))
                 (org-agenda-sorting-strategy '(deadline-up category-up alpha-up))
                 (org-deadline-warning-days 365)
                 (org-agenda-prefix-format "  %(org-format-time-string \"%e %b\" (org-read-date nil t (org-entry-get nil \"DEADLINE\") nil)) %c  %? b")
                 (org-agenda-overriding-header "Deadlines")))
       (tags-todo "inbox"
                  ((org-agenda-prefix-format "  %?-12t% s")
                   (org-agenda-overriding-header "Inbox")))))
     ("r" "Weekly GTD review"
      ((stuck ""
              ((org-agenda-prefix-format "  %b")
               (org-agenda-overriding-header "Stuck Projects")))
       (tags "+project-beta-alpha"
             ((org-agenda-skip-function 'cp-org-skip-not-held)
              (org-agenda-prefix-format "  %b")
              (org-agenda-overriding-header "Held Projects")))
       (todo "HOLD"
             ((org-agenda-skip-function
               '(org-agenda-skip-entry-if 'scheduled 'deadline))
              (org-agenda-prefix-format "  %b")
              (org-agenda-overriding-header "Permanently Held Tasks")))))))

  ;; Don't auto-align tags, ever (for org-modern)
  (org-auto-align-tags nil)
  (org-tags-column 0)

  ;; Miscellaneous UI tweaks
  (org-fontify-whole-heading-line t)
  (org-return-follows-link t)
  
  (org-pretty-entities t)
  ;; sub- and superscripts mess up citations
  (org-pretty-entities-include-sub-superscripts nil)
  (org-hide-emphasis-markers t)
  (org-ellipsis "…")

  (org-startup-with-inline-images t)
  (org-image-actual-width '(300))

  ;; Allow folding to work on start; I understand it
  (org-startup-folded 'showall)

  ;; Open lots of files externally
  (org-file-apps
   (append
    '(;; A few overrides that aren't in my dired config
      ("\\.x?html?\\'" . cp-open-file-string))
    ;; And everything from my dired config
    (mapcar (lambda (ext)
              (cons (concat "\\." ext "\\'")
                    cp-open-file-string))
            cp-external-extensions)
    '(;; Now things that Emacs knows about
      (auto-mode . emacs)
      ;; And open everything else externally by default
      (t . cp-open-file-string))))

  ;; Open internal files in full windows instead of splitting the frame
  (org-link-frame-setup
   '((vm . vm-visit-folder-other-frame)
     (vm-imap . vm-visit-imap-folder-other-frame)
     (gnus . gnus-other-frame)
     (file . find-file)
     (wl . wl-other-frame)))

  ;; Quiet, warning
  (warning-suppress-types '((org-element-cache)))

  ;; Configure various capture templates
  (org-capture-templates '(("t" "Task" entry
                            (file+headline "Agenda/inbox.org" "Inbox")
                            "* TODO %?")
                           ("c" "Calendar" entry
                            (file+headline "Agenda/inbox.org" "Inbox")
                            "* %^{Event name}%?\n%^T")
                           ("C" "Calendar (all-day)" entry
                            (file+headline "Agenda/inbox.org" "Inbox")
                            "* %^{Event name}%?\n%^t")
                           ("n" "Notepad" entry
                            (file "notepad.org")
                            "* %<%Y-%m-%d %H:%M>\n\n%?" :empty-lines 1)
                           ("s" "Student" entry
                            (file "Agenda/students.org")
                            (file "Templates/student.org")
                            :jump-to-captured t
                            :prepend t
                            :empty-lines 1)))

  :config
  ;; Load org-cite support for natbib forms
  (require 'oc-natbib)

  (defun cp-org-show-notepad ()
    "Show my notepad file"
    (interactive)
    (find-file (concat org-directory "/notepad.org")))

  (defun cp-org-show-agenda (arg)
    "Show my custom main agenda view directly"
    (interactive "P")
    (if arg
        (org-agenda)
      (org-agenda nil "c")))

  (defun cp-org-quick-open ()
    "Show a quick-open list for my notepad and all agenda files"
    (interactive)
    (let* ((file-list (append
                       (file-expand-wildcards (concat (car org-agenda-files) "*.org"))
                       (list (concat (file-name-as-directory org-directory) "notepad.org"))))
           (choice (completing-read "Select Org file: " file-list)))
      (when choice
        (find-file choice))))

  (defun cp-org-kill-others (&rest r)
    (delete-other-windows)
    (advice-remove #'org-capture-place-template #'cp-org-kill-others))

  (defun cp-org-kill-frame (&rest r)
    (delete-frame)
    (remove-hook 'org-capture-after-finalize-hook #'cp-org-kill-frame))

  (defun cp-org-full-capture-task ()
    "Call org-capture for a task in a full frame, for CLI usage"
    (interactive)
    (advice-add #'org-capture-place-template :after #'cp-org-kill-others)
    (add-hook 'org-capture-after-finalize-hook 'cp-org-kill-frame)
    (org-capture nil "t"))

  (defun cp-org-add-newline (orig-fun &rest args)
    "Add a newline after the events for a day's agenda for spacing."
    (concat (apply orig-fun args) "\n"))

  (advice-add 'org-agenda-finalize-entries :around #'cp-org-add-newline)

  (defun cp-org-full-capture-calendar ()
    "Call org-capture for a calendar event in a full frame, for CLI usage"
    (interactive)
    (calendar)
    (delete-other-windows)
    (advice-add #'org-capture-place-template :after #'cp-org-kill-others)
    (add-hook 'org-capture-after-finalize-hook 'cp-org-kill-frame)
    (org-capture nil "c"))
  
  ;; Coincidentally, if you apply this to *all* levels of header, including both
  ;; the level-1 headers for projects and the level-N headers for tasks, it will
  ;; show the HOLD tasks in the overview along with the projects themselves
  (defun cp-org-skip-not-held ()
    "Skip projects that aren't held"
    (let* ((subtree-end (save-excursion (org-end-of-subtree t)))
           ;; First step: look forward for a HOLD task
           (is-hold (save-excursion (re-search-forward "^\\*+[ \t]+HOLD" subtree-end t)))
           ;; Second step: look forward for a NEXT task
           (is-next (save-excursion (re-search-forward "^\\*+[ \t]+NEXT" subtree-end t))))
      (if (and is-hold (not is-next))
          nil           ; it's got a hold and no next, so it's a held project
        subtree-end)))  ; it's not a held project, skip it

  (defun cp-org-show-review (&optional arg)
    "Show my custom review agenda view directly"
    (interactive "P")
    (org-agenda arg "r")
    (rename-buffer "*Weekly Review*"))

  (defun org-agenda-color-category (category backcolor forecolor)
    "Color entries in org-agenda by their category"
    (let ((re (rx-to-string `(seq bol (0+ space) ,category space))))
      (save-excursion
        (goto-char (point-min))
        (while (re-search-forward re nil t)
          (add-text-properties (match-beginning 0) (match-end 0)
                               (list 'face (list :background backcolor :foreground forecolor)))))))

  (defun cp-org-color-categories ()
    "Set my agenda category colors"
    (org-agenda-color-category "Inbox:" "#ffffff" "#cc0000")
    (org-agenda-color-category "Home:" "#9ab2d2" "#333333")
    (org-agenda-color-category "Shared:" "#b9cfe7" "#333333")
    (org-agenda-color-category "Work:" "#e68080" "#333333")
    (org-agenda-color-category "Admin:" "#d28080" "#333333")
    (org-agenda-color-category "Groups:" "#e7ae80" "#333333")
    (org-agenda-color-category "Teaching:" "#fed79f" "#333333")
    (org-agenda-color-category "Students:" "#f6ea80" "#333333")
    (org-agenda-color-category "Projects:" "#a7cd83" "#333333")
    (org-agenda-color-category "Holiday:" "#e9ebe7" "#333333"))

  ;; A hook for my agenda buffers of to-do and calendar items
  (defun cp-org-agenda-hook (level)
    ;; I only like to indent content in to-do/agenda buffers
    (org-indent-mode)

    ;; Automatically revert agenda buffers; otherwise we get weird stale data
    (auto-revert-mode)

    ;; Switch off special header fonts at the level specified for a "plain"
    ;; to-do list item
    (when (<= level 1) (face-remap-add-relative 'org-level-1 'bold 'default))
    (when (<= level 2) (face-remap-add-relative 'org-level-2 'bold 'default))
    (when (<= level 3) (face-remap-add-relative 'org-level-3 'bold 'default))
    (face-remap-add-relative 'org-level-4 'bold 'default)
    (face-remap-add-relative 'org-level-5 'bold 'default)
    (face-remap-add-relative 'org-level-6 'bold 'default)
    (face-remap-add-relative 'org-level-7 'bold 'default)
    (face-remap-add-relative 'org-level-8 'bold 'default)

    ;; Reset the visibility back to defaults (fixes a problem with folding the
    ;; custom-commands comment at the end of the file)
    (org-cycle-set-startup-visibility))

  (add-to-list 'safe-local-variable-values '(eval cp-org-agenda-hook 1))
  (add-to-list 'safe-local-variable-values '(eval cp-org-agenda-hook 2))
  (add-to-list 'safe-local-variable-values '(eval cp-org-agenda-hook 3))
  (add-to-list 'safe-local-variable-values '(eval cp-org-agenda-hook 4))

  ;; Save everything after refiling and archiving, without asking me
  (advice-add 'org-refile :after #'org-save-all-org-buffers)
  (advice-add 'org-archive-subtree :after #'org-save-all-org-buffers))
