(use-package recentf
  :custom
  (recentf-max-menu-items 25)
  (recentf-max-saved-items 500)

  (recentf-save-file (expand-file-name "~/.cache/emacs/recentf"))

  :config
  ;; Recent file mode
  (recentf-mode 1)

  ;; Put dired directories into the recentf list
  (defun recentf-add-dired-directory ()
    (when (and (stringp dired-directory)
               (equal "" (file-name-nondirectory dired-directory)))
      (recentf-add-file dired-directory)))
  (add-hook 'dired-mode-hook 'recentf-add-dired-directory))
