(use-package python
  :mode ("\\.py[iw]?\\'" . python-ts-mode)
  :interpreter ("python[0-9.]*" . python-ts-mode)
  :hook (;; `pacin tree-sitter-python-git`
         (python-ts-mode . cp-code-mode-hook)
         (python-ts-mode . cp-python-mode-hook)
         ;; `pipx install pyright` to install the language server
         (python-ts-mode . eglot-ensure))
  :init
  (defun cp-python-mode-hook ()
    ;; Black formats code to 4-spaces, follow it here
    (setq-local tab-width 4)))

(setq package-selected-packages (cons 'python-docstring package-selected-packages))
(use-package python-docstring
  :ensure t
  :hook ((python-ts-mode . python-docstring-mode))
  :custom
  (python-docstring-sentence-end-double-space nil))
