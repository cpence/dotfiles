(use-package message
  :hook ((message-mode . jinx-mode)
         (message-mode . typo-mode))
  :custom
  ;; Forward messages as MIME, which is much more reliable
  (message-forward-as-mime t)
  (message-forward-show-mml 'best)

  ;; I'm not using mailabbrev or ecomplete, switch off support
  (message-mail-alias-type nil)

  ;; Signature
  (message-signature t)
  (message-signature-file "~/.signature")

  ;; This is half of our ignoring format=flowed, the rest of the magic happens
  ;; in the mml configuration
  (message-fill-column nil)

  ;; Prune any adresses from reply-all that are mine
  (message-dont-reply-to-names #'mu4e-personal-or-alternative-address-p)
  
  ;; Play nicely with msmtp and queueing
  (message-send-mail-function 'message-send-mail-with-sendmail)
  (message-sendmail-extra-arguments '("--read-envelope-from"))
  (message-sendmail-f-is-evil t)

  ;; Use the "usual" replied-message quoting format
  (message-citation-line-format "On %a %d %b %Y at %R, %f wrote:\n")
  (message-citation-line-function 'message-insert-formatted-citation-line)

  ;; Try to strip signatures from replies
  (message-cite-function 'message-cite-original-without-signature)

  ;; Use the "usual" forward subject line
  (message-make-forward-subject-function '(message-forward-subject-fwd))

  ;; Don't leave extra buffers around
  (message-kill-buffer-on-exit t))

(use-package gnus-art
  :commands gnus-mime-button-map
  ;; Rebind the "replace mime part" button, which I never use, to the "archive"
  ;; button, which I use constantly
  :bind (:map gnus-mime-button-map
              ("r" . mu4e-view-mark-for-refile)))

(use-package sendmail
  :ensure nil
  :custom
  (sendmail-program "msmtpq"))
