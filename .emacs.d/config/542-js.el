(use-package js-mode
  :mode ("\\.js[mx]?\\'" . js-ts-mode)
  :hook (;; `pacin tree-sitter-javascript-git`
         (js-ts-mode . cp-code-mode-hook)
         ;; `pacin typescript-language-server` to install the language server
         (js-ts-mode . eglot-ensure))
  :custom
  (js-indent-level 2))
