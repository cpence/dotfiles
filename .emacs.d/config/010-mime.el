(use-package emacs
  :init
  ;; Define a global list of extensions to open externally
  (defconst cp-external-extensions
    '(;; PDF and friends
      "pdf" "dvi" "ps" "xps" "djvu" "epub"
      ;; Images
      "jpg" "jpeg" "png" "tif" "tiff" "gif" "svg"
      ;; Office docs
      "rtf" "doc" "docx" "odt" "xls" "xlsx" "ods" "csv" "tsv" "ppt" "pptx"
      "odp"
      ;; Audio
      "wav" "ogg" "mp3" "flac" "m4a" "opus"
      ;; Video
      "mov" "avi" "mkv" "mp4" "mpg" "mpeg" "webm"))

  ;; Run call-process to open a file externally
  (defun cp-open-file (filename)
    (call-process "handlr" nil 0 nil "open" filename))

  ;; Same thing as the above, but as a command string
  (defconst cp-open-file-string "handlr open %s"))
