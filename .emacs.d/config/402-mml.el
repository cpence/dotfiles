(use-package mml
  :delight mml-mode
  :custom
  ;; Set a massive fill column, since format=flowed is essentially a dead standard
  ;; (see https://vxlabs.com/2019/08/25/format-flowed-with-long-lines/)
  (mml-enable-flowed t)
  (fill-flowed-encode-column 998)

  :config
  ;; Always put attachments at the end of the buffer, because otherwise MIME gets
  ;; weird; see http://mbork.pl/2015-11-28_Fixing_mml-attach-file_using_advice
  (defun cp-mml-attach-file--go-to-eob (orig-fun &rest args)
    "Go to the end of buffer before attaching files."
    (save-excursion
      (save-restriction
        (widen)
        (goto-char (point-max))
        (apply orig-fun args))))

  (advice-add 'mml-attach-file :around #'cp-mml-attach-file--go-to-eob))
