;; Configure authinfo.gpg
(use-package auth-source
  :custom
  (auth-sources '("~/.authinfo.gpg"))
  (auth-source-gpg-encrypt-to '("charles@charlespence.net")))
