(use-package emacs
  :bind ("C-x C-o" . cp-switch-to-minibuffer)
  :config
  ;; Shorten all yes/no prompts
  (defalias 'yes-or-no-p 'y-or-n-p)

  (defun cp-switch-to-minibuffer ()
    "Switch to any open minibuffer window."
    (interactive)
    (when (active-minibuffer-window)
      (select-frame-set-input-focus (window-frame (active-minibuffer-window)))
      (select-window (active-minibuffer-window)))))
