(setq package-selected-packages (cons 'yankpad package-selected-packages))
(use-package yankpad
  :ensure t
  :custom
  (yankpad-file (concat org-directory "/yankpad.org"))
  :bind ("C-c y" . cp-yankpad-insert-no-category)
  :config
  (defun cp-yankpad-insert-no-category ()
    "Call (yankpad-insert) but without saving a per-file category "
    (interactive)
    (setq yankpad-category nil)
    (yankpad-insert)))
