(setq package-selected-packages (cons 'all-the-icons package-selected-packages))
(use-package all-the-icons
  :ensure t
  :if (display-graphic-p))

(use-package emacs
  :config
  ;; Default frame fonts
  (set-face-attribute 'default nil :family "IntoneMono Nerd Font" :height 138)
  (set-face-attribute 'fixed-pitch nil :family "IntoneMono Nerd Font Mono" :height 138)
  (set-face-attribute 'variable-pitch nil :family "Helvetica Now Text" :height 140)

  ;; Make sure that all-the-icons loads in, because it has weird autoloads
  (when (display-graphic-p)
    (require 'all-the-icons)))
