(use-package cmake-ts-mode
  :hook (cmake-ts-mode . cp-code-mode-hook)
  :mode ("\\(?:CMakeLists\\.txt\\|\\.cmake\\)\\'" . cmake-ts-mode))
