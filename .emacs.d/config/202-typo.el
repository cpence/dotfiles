(setq package-selected-packages (cons 'typo package-selected-packages))
(use-package typo
  :ensure t
  :delight (typo-mode "  ")
  :bind (:map typo-mode-map
              ("S-<f8>" . cp-toggle-typo-language))
  :config
  (setq-default typo-language "English")

  (defun cp-toggle-typo-language ()
    "Toggle punctuation language for Typo between EN and FR"
    (interactive)
    (let ((typo (if (string= typo-language "English") "French" "English")))
      (setq typo-language typo)
      (setq-default typo-language typo)
      (message "Set punctuation language to %s" typo-language))))
