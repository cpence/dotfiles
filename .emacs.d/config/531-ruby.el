(use-package ruby-ts-mode
  :mode ("\\(?:\\.\\(?:rbw?\\|ru\\|rake\\|thor\\|jbuilder\\|rabl\\|gemspec\\|podspec\\)\\|/\\(?:Gem\\|Rake\\|Cap\\|Thor\\|Puppet\\|Berks\\|Brew\\|Vagrant\\|Guard\\|Pod\\)file\\)\\'" .
         ruby-ts-mode)
  :hook (;; `pacin tree-sitter-ruby-git`
         (ruby-ts-mode . cp-code-mode-hook)
         ;; `gem instal solargraph` to install the language server
         (ruby-ts-mode . eglot-ensure))
  :config
  ;; Add syntax_tree formatter to apheleia
  (push '(syntax_tree . ("stree" "format"
                         (apheleia-formatters-fill-column "--print-width")))
        apheleia-formatters)
  (setf (alist-get 'ruby-mode apheleia-mode-alist)
        'syntax_tree)
  (setf (alist-get 'ruby-ts-mode apheleia-mode-alist)
        'syntax_tree))
