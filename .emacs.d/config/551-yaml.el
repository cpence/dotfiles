(use-package yaml-ts-mode
  :mode ("\\.\\(e?ya?\\|ra\\)ml\\'" . yaml-ts-mode)
  :hook (;; `pacin tree-sitter-yaml-git`
         (yaml-ts-mode . cp-code-mode-hook)
         ;; `pacin yaml-language-server` to install the language server
         (yaml-ts-mode . eglot-ensure)))
