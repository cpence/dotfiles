(use-package dired
  :bind
  (("<f1>" . cp-dired)
   ("S-<f1>" . cp-dired-single)
   :map dired-mode-map
   ;; Clean up the three thousand "file open" keys
   ("RET" . cp-dired-find-file)
   ("o" . cp-dired-find-file-other-window)
   ("<double-mouse-1>" . cp-dired-mouse-find-file)
   ("<mouse-1>" . nil)
   ("<mouse-2>" . nil)
   ("f" . nil)
   ("e" . nil)
   ("v" . nil)
   ;; Don't worry about find-alternate-file
   ("a" . cp-dired-toggle-dotfiles)
   ;; Rebind reload from "g" to "r"
   ("r" . revert-buffer)
   ("g" . nil)
   ;; Bind slash to "go to directory", aka open a new dired buffer
   ("/" . dired)
   ;; Bind capital J to "jump", aka bookmarks
   ;; Don't use consult-bookmark, because it creates a new bookmark if I make a
   ;; typo
   ("J" . bookmark-jump)
   ;; Open this directory in the other window (same command works both ways, but
   ;; I forget if I don't bind it to both keys)
   (">" . cp-dired-send-to-other-window)
   ("<" . cp-dired-send-to-other-window)
   ;; Open a terminal in this folder
   ("T" . cp-dired-terminal-here)
   ;; Copy-to-here and move-to-here
   ("<C-S-c>" . cp-dired-copy-here)
   ("<C-S-r>" . cp-dired-move-here)
   ;; There's a whole pile of dired functionality that I don't use; unbind it so
   ;; I don't use it by accident
   ("i" . nil)
   ("l" . nil)
   ("k" . nil))
  :hook ((dired-mode . auto-revert-mode)
         (dired-mode . dired-hide-details-mode)
         (dired-mode . turn-on-gnus-dired-mode))
  :config
  ;; Configure switches
  (defconst cp-switches-default "--group-directories-first -lvh")
  (defconst cp-switches-all (concat cp-switches-default "a"))

  (setq dired-listing-switches cp-switches-default)

  :init
  (defun cp-dired ()
    "Open up the file manager with two side-by-side panes"
    (interactive)
    ;; If we're currently in dired mode, then open the split pane to the
    ;; directory open in the current window; otherwise open dired in this window
    (let* ((already-open (eq major-mode 'dired-mode))
           (current-dir (if already-open
                            (expand-file-name default-directory)
                          (expand-file-name "~/Nextcloud"))))
      (unless (eq major-mode 'dired-mode)
        (dired current-dir))
      (split-window-right)
      (dired current-dir)
      ;; If we already had dired open, then set focus to the new window
      (if already-open
          (other-window 1))))

  (defun cp-dired-single ()
    "Open up the file manager with a single full-screen pane"
    (interactive)
    (dired (expand-file-name "~/Nextcloud")))

  (defun cp-dired-toggle-dotfiles ()
    "Toggle `dired-listing-switches` between showing dotfiles and not"
    (interactive)
    (if (string= dired-listing-switches cp-switches-all)
        (setq dired-listing-switches cp-switches-default)
      (setq dired-listing-switches cp-switches-all))
    (dired-sort-other dired-listing-switches))

  (defun cp-dired-open-file (file find-file-func)
    "Either open the file externally, or fall back to
 `find-file-func'. The list of file types that will be opened
 externally is governed by `cp-external-extensions'."
    (if (member (file-name-extension file) cp-external-extensions)
        (progn
          (message "Opening %s externally..." file)
          (cp-open-file file))
      (funcall find-file-func file)))

  (defun cp-dired-mouse-find-file (event)
    "Call `cp-dired-open-file' from a mouse click event."
    (interactive "e")
    (let (window pos file)
      (save-excursion
        (setq window (posn-window (event-end event))
	            pos (posn-point (event-end event)))
        (if (not (windowp window))
	          (error "No file chosen"))
        (set-buffer (window-buffer window))
        (goto-char pos)
        (setq file (dired-get-file-for-visit)))
      (cp-dired-open-file (file-name-sans-versions file t) 'find-file)))

  (defun cp-dired-find-file ()
    "Call `cp-dired-open-file' on the file at point in dired."
    (interactive)
    (cp-dired-open-file (dired-get-filename nil t) 'find-file))

  (defun cp-dired-find-file-other-window ()
    "Call `cp-dired-open-file' on the file at point in dired,
 opening it in another window if it is opened in Emacs."
    (interactive)
    (cp-dired-open-file (dired-get-filename nil t) 'find-file-other-window))

  (defun cp-dired-send-to-other-window ()
    "Open this directory but in the other window."
    (interactive)
    (cp-dired-open-file (dired-current-directory) 'find-file-other-window))

  (defun cp-dired-terminal-here ()
    "Open a terminal in the currently shown directory."
    (interactive)
    (with-environment-variables (("NO_SCREEN" "1"))
      (start-process "foot" nil "foot" "-D" (dired-current-directory))))

  (defun cp-dired-copy-here (file)
    "Copy a file to this directory."
    (interactive "fCopy file to here: ")
    (copy-file file (dired-current-directory)))

  (defun cp-dired-move-here (file)
    "Move a file to this directory."
    (interactive "fMove file to here: ")
    (rename-file file (dired-current-directory)))
  
  :custom
  ;; No disk free space in the top line
  (dired-free-space nil)

  ;; Be clever if I open split windows
  (dired-dwim-target t)
  
  ;; Move to trash when we delete in dired
  (delete-by-moving-to-trash t))

(setq package-selected-packages (cons 'all-the-icons-dired package-selected-packages))
(use-package all-the-icons-dired
  :ensure t
  :delight all-the-icons-dired-mode
  :hook
  (dired-mode . all-the-icons-dired-mode))


