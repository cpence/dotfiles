(setq package-selected-packages (cons 'markdown-mode package-selected-packages))
(use-package markdown-mode
  :ensure t
  :mode (("README\\.md\\'" . gfm-mode)
	       ("\\.md\\'" . markdown-mode)
	       ("\\.markdown\\'" . markdown-mode))
  :hook ((markdown-mode . jinx-mode)
         (markdown-mode . auto-fill-mode)
         (markdown-mode . typo-mode)
         (markdown-mode . wc-mode))
  :init
  ;; https://github.com/jrblevin/markdown-mode/issues/578#issuecomment-1126380098
  (setq native-comp-deferred-compilation-deny-list '("markdown-mode\\.el$"))
  :custom
  (markdown-coding-system "utf-8")
  (markdown-command "pandoc")
  (markdown-enable-wiki-links t)
  (markdown-asymmetric-header t)

  (markdown-list-indent-width 2))

(setq package-selected-packages (cons 'markdown-toc package-selected-packages))
(use-package markdown-toc
  :ensure t
  :custom
  (markdown-toc-header-toc-start "<!-- markdown-toc-start -->")
  (markdown-toc-header-toc-end "<!-- markdown-toc-end -->"))

