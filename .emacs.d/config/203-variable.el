;; Configuration for variable-pitch-mode
(use-package emacs
  :config  
  (defun cp-unfill-paragraph ()
    "Turn the current paragraph into a single line."
    (interactive)
    (let ((fill-column (point-max)))
      (fill-paragraph nil)))

  (defun cp-unfill-region ()
    (interactive)
    (let ((fill-column (point-max)))
      (fill-region (region-beginning) (region-end) nil)))

  (defun cp-org-get-start ()
    "Return point after the file preamble."
    (org-element-begin
     (org-element-map (org-element-parse-buffer) '(paragraph headline)
       (lambda (item)
         item) nil t)))
  
  (defun cp-unfill-buffer ()
    (interactive)
    (let ((fill-column (point-max)))
      (fill-region
       (if (eq major-mode 'org-mode)
           (cp-org-get-start)
         (point-min))
       (point-max) nil)))

  (defvar-keymap cp-variable-pitch-mode-map
    "M-q" #'cp-unfill-paragraph
    "M-S-q" #'cp-unfill-buffer)

  (define-minor-mode cp-variable-pitch-mode
    "Customized variable-pitch-mode"
    :lighter nil
    :keymap cp-variable-pitch-mode-map

    (variable-pitch-mode 1)
    (auto-fill-mode -1)
    (visual-line-mode 1)

    (face-remap-add-relative 'org-indent :height 80)
    (setq line-spacing 0.2)))
