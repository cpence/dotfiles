(use-package json-ts-mode
  ;; Bigger regex for JSON files borrowed from json-mode
  :mode ("\\(?:\\(?:\\.\\(?:b\\(?:\\(?:abel\\|ower\\)rc\\)\\|json\\(?:ld\\)?\\)\\|composer\\.lock\\)\\'\\)" .
         json-ts-mode)
  :hook (;; `pacin tree-sitter-json-git`
         (json-ts-mode . cp-code-mode-hook)
         ;; `pacin vscode-json-languageserver` to install the language server
         (json-ts-mode . eglot-ensure)))
