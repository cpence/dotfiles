(use-package emacs
  :custom
  ;; I've moved the tree-sitter grammars
  (treesit-extra-load-path '("~/.cache/emacs/tree-sitter"))

  ;; Configure indentation defaults
  (standard-indent 2)

  ;; Autocomplete if we're already indented
  (tab-always-indent 'complete)
  :config
  ;; Yet more indentation defaults
  (setq-default tab-width 2)
  (setq-default indent-tabs-mode nil)

  (defun cp-code-mode-hook ()
    "A generic programming-mode hook that will be called in all language modes"
    ;; Make lots of things do auto-indent
    (electric-indent-local-mode)

    ;; Show an 80-column fill indicator
    (display-fill-column-indicator-mode)))
