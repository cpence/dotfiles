(setq package-selected-packages (cons 'powerline package-selected-packages))
(use-package powerline
  :ensure t
  :config
  ;; Configure the modeline
  (setq-default mode-line-format
                '("%e"
                  (:eval
                   (let* ((active (powerline-selected-window-active))
                          (mode-line-buffer-id (if active 'mode-line-buffer-id 'mode-line-buffer-id-inactive))
                          (mode-line (if active 'mode-line 'mode-line-inactive))
                          (face0 (if active 'powerline-active0 'powerline-inactive0))
                          (face1 (if active 'powerline-active1 'powerline-inactive1))
                          (face2 (if active 'powerline-active2 'powerline-inactive2))
                          (separator-left (intern (format "powerline-arrow-%s"
                                                          (car powerline-default-separator-dir))))
                          (separator-right (intern (format "powerline-arrow-%s"
                                                           (cdr powerline-default-separator-dir))))
                          (lhs (list (or (when buffer-read-only
                                           (powerline-raw "" face0 'l))
                                         (when (buffer-modified-p)
                                           (powerline-raw "●" face0 'l))
                                         (powerline-raw " " face0 'l))
                                     (powerline-buffer-id `(mode-line-buffer-id ,face0) 'l)
                                     (powerline-raw " " face0)
                                     (funcall separator-left face0 face1)
                                     (powerline-major-mode face1 'l)
                                     (powerline-process face1)
                                     (powerline-minor-modes face1 'l)
                                     (when (buffer-narrowed-p)
                                       (powerline-raw "  󱅛" face1 'l))
                                     (powerline-raw " " face1)
                                     (funcall separator-left face1 face2)
                                     (powerline-vc face2 'r)))
                          (rhs (list (powerline-raw global-mode-string face2 'r)
                                     (let ((project (project-current)))
                                       (if project
                                           (powerline-raw (concat " " (project-name project)) face2 'r)))
                                     (funcall separator-right face2 face1)
                                     (powerline-raw "%4l" face1 'l)
                                     (powerline-raw ":" face1 'l)
                                     (powerline-raw "%3c" face1 'r)
                                     (funcall separator-right face1 face0)
                                     (powerline-raw " " face0)
                                     (powerline-raw "%6p" face0 'r)
                                     (powerline-fill face0 0)
                                     )))
                     (concat (powerline-render lhs)
                             (powerline-fill face2 (powerline-width rhs))
                             (powerline-render rhs)))))))
