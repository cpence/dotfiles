(use-package emacs
  :bind (("S-<f12>" . cp-toggle-frame-size))
  :custom
  ;; Title bar
  (frame-title-format "%b - Emacs")

  ;; Remove some extra UI space
  (inhibit-splash-screen t)
  (inhibit-startup-message t)
  (use-dialog-box nil)

  ;; Start up with a keybind reminder
  (initial-scratch-message ";; [f1]  󰙅 Dired  [f2]   Agenda   [f3]  Mail   [f4]  News  [f5]  󰖸 Contacts\n;; [f6]   Notes  [f7]  󱉟 Library  [f8] 󰓆 Spell  [f9] 󰬤 Date  [f10] 󰍉 Dict\n;; [f11]  Full   [f12]  Project\n\n")

  ;; Don't change font size with C-<wheel>
  (mouse-wheel-scroll-amount
   '(1
     ((shift) . hscroll)
     ((meta))
     ((control))))

  ;; Configure display-time-mode
  (display-time-format " %H:%M" "%-I:%M%p")
  (display-time-default-load-average nil)

  :bind (;; Don't change font size with <pinch>
         ("<pinch>" . nil))

  :init
  ;; Initial window size
  (add-to-list 'default-frame-alist '(height . 40))
  (add-to-list 'default-frame-alist '(width . 95))

  ;; Ability to change frame size on keybind
  (defun cp-toggle-frame-size ()
    "Change between two frame widths on keypress"
    (interactive)
    (if (eq (frame-width) 95)
        (set-frame-width (selected-frame) 140)
      (set-frame-width (selected-frame) 95)))
  
  :config
  ;; Highlight current line
  (global-hl-line-mode 1)

  ;; Show time (and, later, timeclock) in modeline
  (display-time-mode 1)
  
  ;; Bar cursor
  (setq-default cursor-type 'bar)

  ;; Set window and frame borders
  (modify-all-frames-parameters
   '((right-divider-width . 20)
     (internal-border-width . 20)))

  ;; No tooltips
  (tooltip-mode nil)

  ;; Don't pop async shell command output forward
  (add-to-list 'display-buffer-alist
               '("*Async Shell Command*" display-buffer-no-window . nil)))
