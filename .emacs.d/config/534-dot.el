(setq package-selected-packages (cons 'graphviz-dot-mode package-selected-packages))
(use-package graphviz-dot-mode
  :ensure t
  :mode
  "\\.gv\\'"
  "\\.dot\\'"
  :hook (graphviz-dot-mode . cp-code-mode-hook)
  :custom
  (graphviz-dot-indent-width 2))
