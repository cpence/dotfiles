(use-package vc-git
  :mode ("COMMIT_EDITMSG\\'" . vc-git-log-edit-mode)
  :config
  (require 'log-edit))
