(setq package-selected-packages (cons 'modus-themes package-selected-packages))
(use-package modus-themes
  :ensure t
  :config
  (setq modus-themes-italic-constructs t
        modus-themes-bold-constructs t
        modus-themes-mixed-fonts t

        modus-themes-headings
        '((0 . (1.5))
          (1 . (1.5))
          (2 . (1.3))
          (3 . (1.1))
          (agenda-date . (light 1.3))
          (agenda-structure . (light 1.8))
          (t . (1.1)))

        modus-themes-common-palette-overrides
        '(;; I don't like to have the fringe visible
          (fringe unspecified)

          ;; Make headings have an overline like in leuven-theme
          (overline-heading-1 border)
          (overline-heading-2 border)
          (overline-heading-3 border)
          (overline-heading-4 border)))

  (load-theme 'modus-operandi-tinted t))
