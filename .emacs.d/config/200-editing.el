(use-package emacs
  :bind (("C-S-k" . cp-delete-line-backward)
         ("C-k" . cp-delete-line)
         ("C-<delete>" . cp-delete-word)
         ("M-d" . cp-delete-word)
         ("M-DEL" . cp-backward-delete-word)
         ("<C-backspace>" . cp-backward-delete-word)
         ("<f9>" . cp-insert-date))
  :custom
  ;; Disable backups and auto-saves
  (backup-inhibited t)
  (make-backup-files nil)
  (create-lockfiles nil)
  (auto-save-default nil)
  (auto-save-list-file-prefix nil)

  ;; One space after period, please
  (sentence-end-double-space nil)

  ;; Don't tell me when you revert a buffer
  (auto-revert-verbose nil)
  
  :config
  ;; When I use things like C-<backspace> or C-k, delete without putting them in
  ;; the kill ring (save that for when I do a real C-w)
  (defun cp-delete-word (arg)
    "Delete characters forward until encountering the end of a word.
With argument, do this that many times.
This command does not push text to `kill-ring'."
    (interactive "p")
    (delete-region
     (point)
     (progn
       (forward-word arg)
       (point))))

  (defun cp-backward-delete-word (arg)
    "Delete characters backward until encountering the beginning of a word.
With argument, do this that many times.
This command does not push text to `kill-ring'."
    (interactive "p")
    (cp-delete-word (- arg)))

  (defun cp-delete-line ()
    "Delete text from current position to end of line char.
This command does not push text to `kill-ring'."
    (interactive)
    (if (eolp)
        ;; At end of line, delete the EOL character
        (delete-char 1)
      ;; Otherwise, delete the rest of the line but not the EOL
      (delete-region
       (point)
       (progn (end-of-line 1) (point)))))

  (defun cp-delete-line-backward ()
    "Delete text between the beginning of the line to the cursor position.
This command does not push text to `kill-ring'."
    (interactive)
    (let (p1 p2)
      (setq p1 (point))
      (beginning-of-line 1)
      (setq p2 (point))
      (delete-region p1 p2)))

  (defun cp-insert-date ()
    "Insert the current date"
    (interactive)
    (insert (format-time-string "%Y-%m-%d")))
  
  ;; Wrap at 80 characters when we wrap
  (setq-default fill-column 80)
  (setq-default display-fill-column-indicator-column 80)

  ;; Always reload buffers if they get changed by Nextcloud
  (global-auto-revert-mode t)

  ;; Default every single encoding system to UTF-8
  (prefer-coding-system 'utf-8)
  (set-default-coding-systems 'utf-8)
  (set-terminal-coding-system 'utf-8)
  (set-keyboard-coding-system 'utf-8)
  (set-selection-coding-system 'utf-8)
  (set-file-name-coding-system 'utf-8)
  (set-clipboard-coding-system 'utf-8)
  (set-buffer-file-coding-system 'utf-8)

  ;; Fix dead-key input
  (require 'iso-transl))

(use-package autorevert
  :delight (auto-revert-mode "  ")
  :init
  (setq auto-revert-verbose nil))
