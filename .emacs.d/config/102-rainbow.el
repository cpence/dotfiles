(setq package-selected-packages (cons 'rainbow-mode package-selected-packages))
(use-package rainbow-mode
  :ensure t
  :delight
  :hook (prog-mode text-mode))

