(use-package eglot
  :custom
  (eglot-autoshutdown t)
  :config
  ;; Switch eglot to basedpyright
  (add-to-list 'eglot-server-programs
               '((python-mode python-ts-mode)
                 "basedpyright-langserver" "--stdio")))
