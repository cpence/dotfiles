(setq package-selected-packages (cons 'trashed package-selected-packages))
(use-package trashed
  :ensure t
  :hook ((trashed-mode . auto-revert-mode))
  :bind
  (;; Only bind in dired-mode
   :map dired-mode-map
   ("X" . trashed)))
