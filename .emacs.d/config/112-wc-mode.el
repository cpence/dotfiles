(setq package-selected-packages (cons 'wc-mode package-selected-packages))
(use-package wc-mode
  :ensure t
  :bind (:map wc-mode-map
              ("C-c C-w w" . nil)
              ("C-c C-w l" . nil)
              ("C-c C-w a" . nil)
              ("C-c C-w c" . nil)
              ("C-c C-w" . nil)
              ("C-c w" . wc-count))
  :custom
  (wc-modeline-format "   %tw"))
