(setq package-selected-packages (cons 'delight package-selected-packages))
(use-package delight
  :ensure t)

;; Diminish a bunch of native modes
(use-package emacs
  :delight
  (eldoc-mode)
  (visual-line-mode)
  (auto-fill-function "  "))

;; Diminish packages that don't load on startup
(use-package face-remap
  :delight buffer-face-mode)

