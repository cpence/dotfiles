(use-package emacs
  :bind
  (("C-c p t" . cp-plan-open)
   ("C-c p x" . cp-plan-publish))
  :init
  (defvar cp-plan-dir "~/Nextcloud/Plan/")

  (defun cp-plan-yesterday ()
    "Get the path to yesterday's .plan file"
    (format-time-string (concat cp-plan-dir "%Y%m%d.txt")
                        (time-subtract (current-time) (days-to-time 1))))
  
  (defun cp-plan-today ()
    "Get the path to today's .plan file"
    (format-time-string (concat cp-plan-dir "%Y%m%d.txt")))

  (defun cp-plan-missing ()
    "Create an empty special buffer reminding me to update my .plan for the day"
    (let ((buf (get-buffer-create "*no .plan*")))
      (switch-to-buffer buf)
      (fundamental-mode)
      (erase-buffer)
      (insert "\nYou have no .plan file created for today!\nPress C-c p t...\n")
      (read-only-mode)))

  (defun cp-plan-open ()
    "Open today's .plan file, regardless of whether or not it exists"
    (interactive)
    ;; If we're in the dashboard, try to put this in the top-right window
    (when (window-in-direction 'right)
      (select-window (window-in-direction 'right))
      (when (window-in-direction 'up)
        (select-window (window-in-direction 'up))))
    (find-file (cp-plan-today))
    (markdown-mode))

  (defun cp-plan-buffer ()
    "Either load today's .plan file or an angry reminder"
    (interactive)
    (if (file-readable-p (cp-plan-today))
        (cp-plan-open)
      (cp-plan-missing)))

  (defun cp-plan-publish ()
    "Call a bash script that publishes my .plan files"
    (interactive)
    (call-process "plan-publish"))

  (defun cp-dashboard ()
    "Set up a set of views for my personal dashboard that I load into every morning"
    (interactive)
    ;; Go to large frame width
    (if (/= (frame-width) 140)
        (set-frame-width (selected-frame) 140))

    ;; Set up the agenda window
    (delete-other-windows)
    (cp-org-show-agenda nil)

    ;; Set up the .plan window
    (split-window-right 95)
    (other-window 1)
    (cp-plan-buffer)

    ;; Split it and show yesterday, if it exists
    (when (file-readable-p (cp-plan-yesterday))
      (split-window-below)
      (other-window 1)
      (find-file (cp-plan-yesterday))
      (markdown-mode))

    ;; Leave with focus on the agenda window
    (select-window (window-in-direction 'left))))
