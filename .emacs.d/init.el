;; Remove extra UI early in load
(tool-bar-mode -1)
(scroll-bar-mode -1)
(menu-bar-mode -1)

;; Move custom file, and ignore it
(setq custom-file "~/.cache/emacs/custom-ignore.el")

;; Load use-package
(require 'package)
(require 'use-package)

;; Enable MELPA
(setq package-selected-packages nil)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)

;; Automatically update packages
(setq package-selected-packages (cons 'auto-package-update package-selected-packages))
(use-package auto-package-update
  :ensure t
  :config
  (setq auto-package-update-delete-old-versions t)
  (setq auto-package-update-hide-results t)
  (auto-package-update-maybe))

;; Load my modular configuration
(let ((config-dir (expand-file-name "config" user-emacs-directory)))
  (when (file-exists-p config-dir)
    (add-to-list 'load-path config-dir)
    (mapc 'load (directory-files config-dir nil "^[^#].*el$"))))

;; Start the emacs server
(use-package server
  :if window-system
  :init (add-hook 'after-init-hook 'server-start t))
