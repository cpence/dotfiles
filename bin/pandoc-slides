#!/bin/bash

if [ "$1" = "--help" -o $# -ne 1 ]; then
    echo "pandoc-slides: build slides and notes using my lecture slides templates"
    echo ""
    echo "Usage: $0 <in.md>"
    exit 1
fi

INFILE="$1"
OUT_SLIDES="$(basename "$1" .md) (Slides).pdf"
OUT_NOTES="$(basename "$1" .md) (Notes).pdf"

pandoc -t beamer \
    --pdf-engine=lualatex \
    -V aspectratio=169 \
    -V theme=Pittsburgh \
    -V fonttheme=structurebold \
    -V mainfont="Montserrat" \
    -V mainfontoptions="Numbers=OldStyle" \
    -V sansfont="Montserrat" \
    -V mathfont="Fira Math" \
    -V mathfontoptions="math-style=upright" \
    -H ~/bin/pandoc-beamer-snippet.tex \
    -H ~/bin/pandoc-slides-snippet.tex \
    --resource-path .:/home/cpence/Nextcloud/Work/Slides \
    "$1" --standalone -o "$OUT_SLIDES"
pandoc -t latex \
    -V beamerarticle \
    --pdf-engine=lualatex \
    -V papersize=a4 \
    -V fontsize=10pt \
    -V geometry=a4paper,hmargin=2cm,vmargin=2cm \
    -V mainfont="Sabon Next LT Pro" \
    -V mainfontoptions="Numbers=OldStyle" \
    -V sansfont="Montserrat" \
    -V mathfont="Euler Math" \
    -V mathfontoptions="math-style=upright" \
    -H ~/bin/pandoc-beamer-snippet.tex \
    -H ~/bin/pandoc-handout-snippet.tex \
    --lua-filter ~/bin/pandoc-handout-filter.lua \
    --resource-path .:/home/cpence/Nextcloud/Work/Slides \
    "$1" --standalone -o "$OUT_NOTES"
