#!/bin/bash
# Check to make sure that there is a "complete set" of extensions for each
# filename in this folder and all sub-folders (rescursively). Call this script
# with all the extensions you expect to find, and it will put any files with
# some extensions missing in a sub-folder called 'orphans'. E.g.,
#
# find-orphans pdf,txt,xml
#
# will find any file for which there pdf, txt, or xml are missing and place the
# existing files into the 'orphans' sub-folder.

if [[ "$1" == "--help" ]]; then
    echo "find-orphans pdf,txt,xml"
    exit 1
fi

# Get comma-separated extensions into an array
readarray -td, extensions <<<"$1,"
unset 'extensions[-1]'

function process_directory() {
    dir=$1
    echo "Processing '$dir'..."

    # Get a full list of unique basenames in this directory
    declare -A contents

    for basename in $(find "$dir" -maxdepth 1 -type f -print0 |
        while IFS= read -r -d '' filename; do
            basename=$(basename $filename)
            echo ${basename%.*}
        done); do
        contents[$basename]=0
    done

    # Check each basename
    for basename in "${!contents[@]}"; do
        for ext in "${extensions[@]}"; do
            to_test="$dir/$basename.$ext"
            if [[ ! -f $to_test ]]; then
                echo "...$basename is an orphan, moving"

                mkdir -p $dir/orphans
                mv $dir/$basename.* $dir/orphans
                break
            fi
        done
    done

    # Recurse into subdirectories
    find "$dir" -maxdepth 1 -type d -print0 |
        while IFS= read -r -d '' subdir; do
            if [[ "$subdir" == "$dir" ]]; then
                continue
            fi
            if [[ $(basename "$subdir") == "orphans" ]]; then
                # Don't process already-processed orphans folders
                continue
            fi

            process_directory "$subdir"
        done
}

process_directory "."
