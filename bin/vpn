#!/bin/bash
# Script to manage my VPN connections.
#
# Run as vpn <profile>.
#
# To add a new profile, use:
# > openvpn3 config-import --config (something.ovpn) \
#     --persistent --name (name)
#
# To set the username and password for the profile:
# secret-tool store --label=vpn-username service vpnuser profile (name)
# secret-tool store --label=vpn-password service vpnpass profile (name)

if [[ "$1" = "--help" || $# -lt 1 ]]; then
  echo "vpn: connect to a saved VPN profile"
  echo ""
  echo "Usage: vpn <profile>"
  exit 1
fi

PROFILE="$1"

# Get the username and password
VPNUSER=`secret-tool lookup service vpnuser profile $PROFILE`
VPNPASS=`secret-tool lookup service vpnpass profile $PROFILE`

if [[ -z "$VPNUSER" || -z "$VPNPASS" ]]; then
    echo "ERROR: Cannot find username or password for this VPN profile!"
    echo "See the top of the script for info about how to save them to the keyring."
    exit 1
fi

# Sanitize the username and password for printf escapes
VPNUSER=`echo "$VPNUSER" | sed 's@%@%%@g'`
VPNPASS=`echo "$VPNPASS" | sed 's@%@%%@g'`

# Feed the username and password into connection
printf "$VPNUSER\n$VPNPASS\n" | openvpn3 session-start -c $PROFILE

# See if this needed a web login
auth_out=`openvpn3 session-auth`
if [[ "$auth_out" != "No sessions requires authentication"* ]]; then
    echo ""
    echo "Waiting for the web authentication to finish..."
    echo ""

    count=0
    while true; do
        sleep 3
        count=$((count + 1))
        auth_out=`openvpn3 session-auth`

        if [[ "$auth_out" == "No sessions requires authentication"* ]]; then
            break
        fi

        if [[ $counter -eq 100 ]]; then
           echo "...Timed out waiting for authentication to finish!"
           exit 1
        fi
    done
fi

# Idle waiting on Ctrl-C from the user
echo ""
echo "Press Ctrl-C to disconnect..."
( trap exit SIGINT ; read -r -d '' _ </dev/tty )
echo ""

# Disconnect
openvpn3 session-manage --disconnect -c $PROFILE
