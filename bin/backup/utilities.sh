#!/bin/bash

# Make sure the NAS is really there
function check_nas {
    if [[ ! -d /mnt/home/Backup ]]; then
        echo "ERROR: NAS is not mounted, mount it and re-run this script"
        exit 1
    fi
}
