\ProvidesClass{ptpb}[2024/11/03 PTPBio article class]
\NeedsTeXFormat{LaTeX2e}

%----------------------------------------------------------------------------------------
% Parse options
%----------------------------------------------------------------------------------------
\RequirePackage{etoolbox}
\newbool{proofstoggle}
\newbool{preprinttoggle}
\newbool{biblatextoggle}
\newbool{bibtextoggle}
\newbool{simpletoggle}
\DeclareOption{proofs}{\booltrue{proofstoggle}}
\DeclareOption{preprint}{\booltrue{preprinttoggle}}
\DeclareOption{biblatex}{\booltrue{biblatextoggle}}
\DeclareOption{bibtex}{\booltrue{bibtextoggle}}
\DeclareOption{simple}{\booltrue{simpletoggle}}
\ProcessOptions\relax

%----------------------------------------------------------------------------------------
% General package dependencies
%----------------------------------------------------------------------------------------
\LoadClass[12pt,a4paper]{article}

\RequirePackage{ragged2e}

\RequirePackage{geometry}
\geometry{reset,a4paper,margin=1in,headsep=10pt,footskip=16pt}

\RequirePackage{graphicx}
\RequirePackage[font=small]{caption}

\RequirePackage{xcolor}
\definecolor{oaorange}{RGB}{235,129,40}
\definecolor{orcidgreen}{cmyk}{.4,0,1,0}

\RequirePackage[stable]{footmisc}
\RequirePackage{mathtools}

\RequirePackage{enumitem}
\RequirePackage{tabularx,longtable,booktabs,multirow}
\setlength{\abovetopsep}{1ex} % space between caption and toprule of table in booktabs

\ifbool{simpletoggle}{
  % Strangely, the hangparas environment does not convert properly. Dummy it out
  % in simple mode.
  \newenvironment{hangparas}[2]{}{}
}{\RequirePackage{hanging}}

% PDF metadata
% ---------------
\ifbool{simpletoggle}{}{
  % We don't even load hyperref when we're in simple mode, as it puts far too
  % many links all over the place.
  \AtEndPreamble{%
    \RequirePackage{hyperref}
    \urlstyle{same} % change from monospace to regular font for URLs
  }

  \AtBeginDocument{%
    \hypersetup{%
      pdftitle={\thetitle},%
      pdfauthor={\theauthor},%
      pdfkeywords={PTPBio, article, philosophy, biology, open access},%
      pdflang={English},%
      hidelinks=true%
    }%
  }
}

% Fonts
%---------------

\ifbool{simpletoggle}{
  \newcommand{\aiOrcid}{}
  \newcommand{\aiOpenAccess}{}
}{
  % We use system fonts through fontspec, because we compile PDFs with LuaTeX.
  \RequirePackage{fontspec}
  \defaultfontfeatures{Ligatures=TeX, Numbers=Lining}
  \setmainfont{Adobe Caslon Pro}

  \RequirePackage{microtype}

  % Use Tex Gyre Pagella Math for most of the math font, except pull the
  % standard digits from Caslon so that they match
  \RequirePackage{unicode-math}
  \setmathfont{texgyrepagella-math.otf}
  \setmathfont[range=\mathup/{num}]{Adobe Caslon Pro}

  \RequirePackage{academicons}

  % Fonts for \maketitle
  \RequirePackage{titling}
  \pretitle{\begin{center}\LARGE\bfseries}
  \posttitle{\par\end{center}}

  % Fonts for section headers
  \RequirePackage{titlesec}
  \titleformat{\section}[hang]{\large\bfseries}{\thesection.}{0.5em}{}
  \titleformat{\subsection}[hang]{\normalsize\mdseries\itshape}{\thesubsection.}{0.5em}{}
  \titleformat{\subsubsection}[hang]{\normalsize\mdseries}{\thesubsubsection.}{0.5em}{}
}

%----------------------------------------------------------------------------------------
% BibLaTeX / BibTeX
%----------------------------------------------------------------------------------------

\ifbool{biblatextoggle}{
  % Load biblatex-chicago, and then customize it a bit
  \RequirePackage[authordate,backend=biber,isbn=false,doi=true,url=false,bibencoding=inputenc,cmsdate=both,compresspages=true,maxbibnames=7,giveninits=false,noibid]{biblatex-chicago}

  % suppress the quotation marks for journal titles
  \DeclareFieldFormat*{journaltitle}{\emph{#1}}
  % suppress “p.” and “pp.” for page numbers
  \DeclareFieldFormat[article]{pages}{#1}
  % suppress access date in field urldate for item-types other than online
  \DeclareFieldFormat[article,book,inbook,incollection,inproceedings,patent,thesis,unpublished]{urldate}{}
  % retain access date in field urldate for item-type online
  \DeclareFieldFormat[online]{urldate}{#1} 
  \DeclareFieldFormat*[article]{number}{#1}
  % suppress month names for journal articles
  \DeclareFieldFormat*[article]{month}{}
  % automatically title-case most title fields
  \DeclareFieldFormat[article,book,inbook,incollection,inproceedings,patent,thesis,unpublished]{titlecase}{\MakeSentenceCase*{#1}}

  % Enabling \global\undef\bbx@lasthash line suppresses the 3 em-dash substitution
  \makeatletter
  \AtEveryBibitem{\global\undef\bbx@lasthash}
  \makeatother
  \bibsetup{
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{5pt}
  }

  % Font size in bibliography
  \renewcommand*{\bibfont}{\RaggedRight \small \sloppy}

  % Correct header for bibliography
  \DefineBibliographyStrings{english}{
    references = {Literature cited},
  }
}{
  % If BibLaTeX isn't enabled, then borrow the footnote formatting from that
  % package so that footnotes are consistent.
  \renewcommand\@makefntext[1]{% Provides in-line footnote marks
    \setlength\parindent{1em}%
    \noindent
    \makebox[2.3em][r]{\@thefnmark.\,\,}#1}

  \ifbool{bibtextoggle}{
    % We have rudimentary support for natbib-based BibTeX bibliographies as
    % well.
    % FIXME: This needs more debugging; it's only really been tested once.
    \RequirePackage{natbib}

    % Same bibliography formatting as the above, only for natbib
    \renewcommand{\refname}{Literature cited}
    \renewcommand{\bibfont}{\RaggedRight \small \sloppy}
    \setlength{\bibhang}{2em}
    \setlength{\bibsep}{5pt}
  }{}
}


%----------------------------------------------------------------------------------------
% Header and footer
%----------------------------------------------------------------------------------------

\ifbool{simpletoggle}{}{
  % Don't redo headers and footers with this fancy information if we're
  % producing simple output.
  \RequirePackage{fancyhdr}
  \pagestyle{fancy}
  \setlength{\headheight}{21pt}

  % Type indicator colors
  \definecolor{articlegreen}{RGB}{217,234,202}
  \definecolor{reviewtan}{RGB}{251,221,146}
  \definecolor{trendsblue}{RGB}{194,216,249}
  \definecolor{specialpurple}{RGB}{231,192,255}
  \definecolor{notered}{RGB}{255,136,130}
  \definecolor{positiongrey}{RGB}{230,230,230}

  % Pages after the first page
  \fancyhf{}
  \lhead{\colorbox{\typecolor}{\normalsize\textsc{\strut~~\runningauthor: \runningtitle~~}}}
  \rhead{\thepage}

  \rfoot{\raisebox{-0.35\height}{\textcolor{oaorange}{\aiOpenAccess{}}
      \textsc{open access - \href{http://ptpbio.org}{ptpbio.org}}}}

  \fancypagestyle{plain}{
    % First page
    \fancyhf{}

    \lhead{\raisebox{-3pt}{\small Philos Theor Pract Biol (\publicationyear) \volumenumber{}\,(\issuenumber): \articlenumber}}
    \rhead{\colorbox{\typecolor}{\textsc{\strut ~~\articletype~~}}}

    \lfoot{\raisebox{-0.5\height}{\includegraphics[height=32pt]{ptpbio-logo.png}}}
    \rfoot{\raisebox{-0.35\height}{\textcolor{oaorange}{\aiOpenAccess{}}
        \textsc{open access - \href{http://ptpbio.org}{ptpbio.org}}}}
  }
}

%----------------------------------------------------------------------------------------
% Title page and affiliations
%----------------------------------------------------------------------------------------

\providecommand{\kwsep}{}

\ifbool{simpletoggle}{
  % In simple mode, we output basic author affiliation footnotes rather than a
  % complex affiliation block.
  \providecommand{\thanksmark}[1]{}
  \renewcommand{\thanksmark}[1]{\footnote{\csuse{aff#1}\email{mail#1}\orcid{orcid#1}}}

  % Put the article type and date info just after the abstract
  \apptocmd{\endabstract}{\noindent Received \receiveddate;
    \ifdef{\reviseddate}{Revised \reviseddate; }{} Accepted \accepteddate \\
    Article Type: \articletype \\
    Keywords: \renewcommand{\kwsep}{; } \keywords \\
    doi:\articledoi}{}{}
}{
  % Print ORCIDs with the logo
  \newcommand{\orcid}[1]{\ifcsdef{#1}{ \enspace \href{https://orcid.org/\csuse{#1}}{\textcolor{orcidgreen}{\aiOrcid}\;https://orcid.org/\csuse{#1}}}{}}
  \newcommand{\email}[1]{\ifcsdef{#1}{, \href{mailto:\csuse{#1}}{\csuse{#1}}}{}}

  % Put a rule after the \maketitle information
  \renewcommand{\maketitlehookd}{%
    \begin{figure}[b!]
      \begin{minipage}[b]{\textwidth}
        \centering
        \hrule\medskip\footnotesize
        % This loops over all of the author metadata entries that are loaded and
        % produces an affiliation line for each one (see corresponding code in
        % the template for the array definition)
        \newcounter{affnum}\setcounter{affnum}{0}%
        \whileboolexpr{ test {\ifnumcomp{\value{affnum}}{<}{\theauth}} }%
        {\stepcounter{affnum}%
          \thanksmark{\value{affnum}}\hspace{4pt}\csuse{aff\theaffnum}%
          \email{mail\theaffnum}\orcid{orcid\theaffnum}\par}
        \medskip
        Received \receiveddate;
        \ifdef{\reviseddate}{Revised \reviseddate; }{}
        Accepted \accepteddate \\
        doi:\href{https://doi.org/\articledoi}{\articledoi}
      \end{minipage}
    \end{figure}
    \vspace{-16pt}%
    \noindent\hfil\rule{0.8\textwidth}{.8pt}\hfil}

  % Pull the abstract up toward that vertical rule
  \renewcommand{\abstractname}{\vspace{-24pt}}

  % Don't indent the abstract
  \patchcmd{\abstract}{\quotation}{\quotation\noindent\ignorespaces}{}{}

  % Put the same rule after the abstract, then the keywords
  \apptocmd{\endabstract}{\noindent \hfil\rule{0.8\textwidth}{.8pt}\hfil\par%
    \begin{center}
      \textbf{Keywords}
      \par\small\medskip
      \renewcommand{\kwsep}{ \textbullet{} } \keywords
      \medskip
    \end{center}}{}{}
}

%----------------------------------------------------------------------------------------
% License information
%----------------------------------------------------------------------------------------

\ifbool{simpletoggle}{}{
  % Print a licensing information block at the end of the article
  \AtEndDocument{
    \bigskip\hrule\smallskip

    \begin{center}\small
      \noindent © \publicationyear{} Author(s) \\[6pt]
      This is an open-access article, licensed under \\
      \href{https://creativecommons.org/licenses/by/4.0/}{Creative Commons
        Attribution 4.0 International}
      \raisebox{-0.15em}{\includegraphics[height=1em]{cc.pdf}}
      \raisebox{-0.15em}{\includegraphics[height=1em]{cc-by.pdf}} \\[4pt]
      This license requires that reusers give credit to the creator(s). It
      allows reusers to \\ distribute, remix, adapt, and build upon the
      material in any medium or format.

      \vspace{\baselineskip}
      ISSN 2475-3025
    \end{center}
  }
}

%%%%%%%%%%%%%%%%%%%
% PROOFS MATERIAL %
%%%%%%%%%%%%%%%%%%%

% If proofs, load line numbers package

\ifbool{proofstoggle}{
\RequirePackage{lineno}

\AfterEndPreamble{
\linenumbers
}
}
{}

%%%%%%%%%%%%%%%%%%%%%
% PREPRINT MATERIAL %
%%%%%%%%%%%%%%%%%%%%%

% If preprint, load line numbers package

\ifbool{preprinttoggle}{
\RequirePackage{lineno}

\AfterEndPreamble{
\linenumbers
}
}
{}

\ifbool{preprinttoggle}{
\RequirePackage[firstpageonly=true]{draftwatermark}

\DraftwatermarkOptions{
angle=0,
vanchor=t,
vpos={7em},
text={PREPRINT \\ When it is published, please cite the final version.},
fontsize={12pt},
alignment=c
}

}
{}

%----------------------------------------------------------------------------------------
% Pandoc template fragments
%----------------------------------------------------------------------------------------
% Some commands will be automatically inserted by Pandoc when it sees certain
% Markdown features, so we need to define them here.
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}

\endinput
