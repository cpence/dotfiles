#!/usr/bin/env python
# Extremely simple yet standards-compliant mail sorter
import re

# Configurable knobs and filters
MAIL_ROOT = "~/Nextcloud/Mail"
MAIL_INBOX = "Inbox"

# If this is set, then print out every matched filter
DEBUG = False
# If this is set, print headers for every message we look at
SUPER_DEBUG = False
# If this is set, don't move stuff
DRY_RUN = False

# If these are variables, then if I misspell them below, it raises an error
FROM = "from"
FROM_NAME = "from-name"
ALL = "all"
LIST_ID = "list-id"
SUBJECT = "subject"

TRASH = "Trash"
NEWS = "News"

# This is a super simple filter format. Each filter is a three-element tuple.
# The first element is the header that you want. If it's one specific header,
# like "from", "to", "subject", etc., then it will match that header only. If
# it's "all", then we search To, Cc, Resent-To, and Resent-Cc.
#
# The second element is tested against all email addresses in that header, and
# if one address contains the pattern mentioned, the message will be moved into
# the mailbox specified in the third element. The match is case-insensitive.
#
# So ("all", "johndoe.com", "Trash") will move a message to Trash if any of the
# headers mentioned above contain "johndoe.com".
#
# If there is a fourth element in the tuple, then it is a keyword that will be
# added to the headers of every message that matches this rule.
FILTERS: list[tuple[str, str, str] | tuple[str, str, str, str]] = [
    # This is handled specially below
    (FROM, "rss2email@swab.lu", NEWS, ""),
    # Instant trashing
    (FROM, "l.mpence@textnow.me", TRASH),
    (FROM, "l.mpence@yahoo.com", TRASH),
    (FROM, "gmacquisitions@gmail.com", TRASH),
    (FROM, "chengji@missouri.edu", TRASH),
    (FROM, "acm.bcb.sigbio@gmail.com", TRASH),
    (FROM, "kristen.pittenger@kering.com", TRASH),
    (FROM, "jthorn5656@gmail.com", TRASH),
    (FROM, "urianrios@outlook.com", TRASH),
    (FROM, "michaelochoa@mac.com", TRASH),
    (FROM, "customerservice@ehealthinsurance.com", TRASH),
    (FROM, "hufspr@hufs.ac.kr", TRASH),
    (FROM, "michael.lissack@gmail.com", TRASH),
    (FROM, "wwbw.com", TRASH),
    (FROM, "davidpublishing.org", TRASH),
    (FROM, "davidpublishing.com", TRASH),
    (FROM, "walgreens.com", TRASH),
    (FROM, "adt.com", TRASH),
    (FROM, "newsletter.dickieslife.com", TRASH),
    (FROM, "heythere@m.redbubble.com", TRASH),
    (ALL, "bitbucket@charlespence.net", TRASH),
    (ALL, "mentor-l@list.unm.edu", TRASH),
    (ALL, "objecteursdecroissance.be", TRASH),
    (SUBJECT, "You've got a money request", TRASH),
    #
    # News: Philosophy
    (ALL, "BSTS@LS.KULEUVEN.BE", NEWS, "philosophy.bsts"),
    (FROM_NAME, "Daily Nous", NEWS, "philosophy.dailynous"),
    (LIST_ID, "filos-nl.", NEWS, "philosophy.filos-nl"),
    (ALL, "gohkust.onmicrosoft.com", NEWS, "philosophy.hkust"),
    (FROM, "newsletter@philinbiomed.org", NEWS, "philosophy.pibm"),
    (LIST_ID, "hopos-g.vt.edu", NEWS, "philosophy.hopos"),
    (ALL, "hpsst-list@groups.unsw.edu.au", NEWS, "philosophy.hpsst"),
    (FROM, "ndpredit@nd.edu", NEWS, "philosophy.ndpr"),
    (ALL, "PHILOS-L@liverpool.ac.uk", NEWS, "philosophy.philos-l"),
    (ALL, "PHILOS-L@LISTSERV.LIV.AC.UK", NEWS, "philosophy.philos-l"),
    #
    # News: History
    (FROM, "info@chstm.org", NEWS, "history.cshtm"),
    (ALL, "MERSENNE@JISCMAIL.AC.UK", NEWS, "history.mersenne"),
    (LIST_ID, "metzger.groupes.renater.fr", NEWS, "history.metzger"),
    (LIST_ID, "dept3_externals.listserver.mpiwg-berlin.mpg.de", NEWS, "history.mpiwg"),
    (LIST_ID, "sarton.lists.ugent.be", NEWS, "history.sarton"),
    (FROM, "spreading@univ-lille.fr", NEWS, "history.spreading"),
    (FROM, "historyofknowledge.utrecht@gmail.com", NEWS, "history.utrecht"),
    #
    # News: Biology
    (FROM, "sea-phsi@bristol.ac.uk", NEWS, "biology.bristol"),
    (FROM, "info@futurumcareers.com", NEWS, "biology.futurum"),
    #
    # News: DH
    (LIST_ID, "institute.lists.uvic.ca", NEWS, "dh.dhsi"),
    (ALL, "mlg-ulb@googlegroups.com", NEWS, "dh.ml-ulb"),
    (FROM, "ulbmlgmanager@", NEWS, "dh.ml-ulb"),
    #
    # News: Societies
    (FROM, "boletim@abfhib.org", NEWS, "societies.abfhib"),
    (FROM, "info@academieroyale.be", NEWS, "societies.arb"),
    (FROM, "cshl.edu", NEWS, "societies.cshl"),
    (FROM, "info@hssonline.org", NEWS, "societies.hss"),
    (ALL, "ISHPSB-L@LISTS.UMN.EDU", NEWS, "societies.ishpssb"),
    (FROM, "secretary@ishpssb.org", NEWS, "societies.ishpssb"),
    (FROM, "news@ishpssb.org", NEWS, "societies.ishpssb"),
    (FROM, "epsa@wildapricot.org", NEWS, "societies.epsa"),
    (FROM, "cnrlncnl@logic-center.be", NEWS, "societies.logic-be"),
    (FROM, "media@ncse.ngo", NEWS, "societies.ncse"),
    (FROM, "govdelivery.nsf.gov", NEWS, "societies.nsf"),
    (FROM, "office@philsci.org", NEWS, "societies.psa"),
    (LIST_ID, "sbp-info.freelists.org", NEWS, "societies.sbp"),
    (FROM, "contact@shesvie.fr", NEWS, "societies.shesvie"),
    (FROM, "adhesion@sps-philoscience.org", NEWS, "societies.sps"),
    #
    # News: Journals
    (FROM, "no-reply@biomedcentral.com", NEWS, "journals.bmc"),
    (FROM, "noreply@press.uchicago.edu", NEWS, "journals.chicago"),
    (FROM, "sciencedirect@notification.elsevier.com", NEWS, "journals.elsevier"),
    (FROM, "nationalacademiespress@nas.edu", NEWS, "journals.nas"),
    (FROM, "NationalAcademiesPress@NationalAcademies.org", NEWS, "journals.nas"),
    (FROM, "springer@alerts.springer.com", NEWS, "journals.springer"),
    (FROM, "tocalert@springernature.com", NEWS, "journals.springer"),
    #
    # News: News
    (FROM, "democratsabroad.org", NEWS, "news.demsabroad"),
    (FROM, "lalibre.be", NEWS, "news.lalibre"),
    (FROM, "tom@moylan.eu", NEWS, "news.restless"),
    #
    # News: Princeton
    (FROM, "pawgrad@princeton.edu", NEWS, "princeton.alums"),
    (FROM, "eventmgr@princeton.edu", NEWS, "princeton.alums"),
    (ALL, "pu-band-alums@princeton.edu", NEWS, "princeton.pub-alums"),
    (FROM, "ConnectedCommunity.org", NEWS, "princeton.uk"),
    #
    # News: Blogs
    (FROM_NAME, "Quomodocumque", NEWS, "blogs.ellenberg"),
    (FROM, "slcgallery@lassospace.com", NEWS, "blogs.steveclark"),
    #
    # News: Comics
    (FROM, "smbc-comics.com", NEWS, "comics.smbc"),
    (FROM, "xkcd.com", NEWS, "comics.xkcd"),
    #
    # News: Tech
    (FROM, "schneier@schneier.com", NEWS, "tech.schneier"),
    (LIST_ID, "Discussion.Codeberg-e.V..codeberg.org", NEWS, "tech.codeberg"),
    #
    # News: Games
    (FROM, "nintendo.com", NEWS, "games.nintendo"),
    (FROM, "nintendo-europe.com", NEWS, "games.nintendo"),
    (FROM, "playstation.com", NEWS, "games.sony"),
    (FROM, "purchase-noreply@twitch.tv", NEWS, "games.twitch"),
    (FROM, "itch.io", NEWS, "games.itch"),
    (FROM, "steampowered.com", NEWS, "games.steam"),
    #
    # News: Sports
    (FROM, "info@unionsg.news-mailer.eu", NEWS, "sports.rusg"),
    (FROM, "editorial.theguardian.com", NEWS, "sports.guardian"),
    (FROM, "sbnnewsletters@sbnation.com", NEWS, "sports.wagnh"),
    (FROM_NAME, "CHELSEA/esque", NEWS, "sports.axon"),
    #
    # News: Misc
    (FROM, "americanclubbrussels.org", NEWS, "misc.americanclub"),
    (FROM, "americanclubbrussels@wildapricot.org", NEWS, "misc.americanclub"),
]


from email.header import decode_header, make_header
from email.utils import getaddresses
from mailbox import Maildir

# Load the mailbox
from pathlib import Path

inbox_path = (Path(MAIL_ROOT) / MAIL_INBOX).expanduser()
box = Maildir(inbox_path)


def convert_header(h):
    try:
        return str(make_header(decode_header(h)))
    except UnicodeDecodeError:
        # Curse you, malformed emails
        return ""


def get_header(message, header):
    if header not in message:
        return None

    h = convert_header(message[header])
    return h.replace("\n", "")


# This function assumes that addresses and test have already been sent through
# casefold() so that we can do case-insensitive matching
def matches(strs, test):
    if strs is None:
        return False
    if test is None:
        return False

    for s in strs:
        if test in s:
            return True

    return False


for key, message in box.items():
    # This is an internal implementation detail of the Mailbox class, but we
    # need it to be able to move files
    msg_path = box._lookup(key)
    src_path = Path(MAIL_ROOT).expanduser() / MAIL_INBOX / msg_path

    # Get all of the headers that we need
    from_headers = getaddresses(message.get_all("from", []))
    if len(from_headers) == 0:
        print(f"WARNING: Message {msg_path} has no from address?")

    frm = [addr[1] for addr in from_headers]
    frm_names = [convert_header(addr[0]) for addr in from_headers]

    subject = get_header(message, "subject")
    list_id = get_header(message, "list-id")

    tos = message.get_all("to", [])
    ccs = message.get_all("cc", [])
    resent_tos = message.get_all("resent-to", [])
    resent_ccs = message.get_all("resent-cc", [])
    all_recipients = getaddresses(tos + ccs + resent_tos + resent_ccs)

    if len(all_recipients) == 0:
        print(f"WARNING: Message {msg_path} has no recipients?")
        all_recipients = None
    else:
        all_recipients = [addr[1] for addr in all_recipients]

    # Run all of these header values through casefold and make them into arrays
    if frm is not None:
        frm = [addr.casefold() for addr in frm]
    if frm_names is not None:
        frm_names = [addr.casefold() for addr in frm_names]
    if list_id is not None:
        list_id = [list_id.casefold()]
    if all_recipients is not None:
        all_recipients = [addr.casefold() for addr in all_recipients]

    # Print super-debugging if needed
    if SUPER_DEBUG is True:
        print(frm)
        print(list_id)
        print(all_recipients)
        print(subject)
        print("------")

    # Do the filtering
    for tup in FILTERS:
        # Manual unpacking, since these have different lengths
        header = tup[0]
        pattern = tup[1]
        dest = tup[2]
        if len(tup) == 4:
            tag = tup[3]
        else:
            tag = None

        pattern = pattern.casefold()

        match = False
        if header == FROM:
            match = matches(frm, pattern)
        elif header == FROM_NAME:
            match = matches(frm_names, pattern)
        elif header == ALL:
            match = matches(all_recipients, pattern)
        elif header == LIST_ID:
            match = matches(list_id, pattern)
        elif header == SUBJECT:
            # Handle subject a bit differently for rewriting below
            match = matches([subject.casefold()], pattern)

        if match and DEBUG:
            print(f"DEBUG: Message {msg_path} matched rule " + f"({header}, {pattern}, {dest})")

        if match is False:
            continue

        # Override tag for rss2email messages
        if pattern == "rss2email@swab.lu":
            if subject is None:
                print("WARNING: rss2email message without subject")
                continue

            m = re.match(r"R2E ([a-z0-9.]+) \| (.*)", subject)
            if m is None:
                print("WARNING: rss2email message without feed tag")
                continue

            tag = m.group(1)

            # Rewrite subject to pull off the tag
            subject = m.group(2)
            del message["subject"]
            message["subject"] = subject

        # If we have a tag, add it as a keyword
        if tag:
            if "x-keywords" in message:
                del message["x-keywords"]
            message["x-keywords"] = tag

            # Force an update of this message on disk
            box[key] = message

        # Okay, move the message
        dest_path = Path(MAIL_ROOT).expanduser() / dest
        if not dest_path.exists():
            print(f"ERROR: Destination folder {dest_path} does not exist")
            continue

        dest_path /= msg_path

        if DRY_RUN:
            print(f"DEBUG: Would move {src_path} to {dest_path}")
        else:
            if dest_path.exists():
                print(f"ERROR: Cannot move {src_path} to {dest_path}:" + " destination already exists")
            else:
                src_path.rename(dest_path)

                # Only act on one filter at most per message, in order
                break


# Local Variables:
# fill-column: 999
# End:
