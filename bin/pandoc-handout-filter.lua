if FORMAT:match 'latex' then

  local function latex(str)
    return pandoc.RawInline('latex', str)
  end

  function Div(el)
    if el.classes:includes("notes") then
      -- insert element in front
      table.insert(
        el.content, 1,
        pandoc.RawBlock("latex", "\\begin{slidenotes}"))
      -- insert element at the back
      table.insert(
        el.content,
        pandoc.RawBlock("latex", "\\end{slidenotes}"))
    end
    return el
  end

end
