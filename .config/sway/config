# Run sway's bundled dbus configuration, config GTK
include /etc/sway/config.d/*
exec_always ~/bin/gsettings-import

# Configure displays and background
exec kanshi
exec_always kanshictl reload
exec sh ~/.azotebg

# Background daemons
exec lxpolkit
exec ~/bin/swaysetlock
exec dunst
exec liquidctl initialize
# FIXME: This is hardcoded to Brussels
exec gammastep -l 50.85045:4.34878 -t 6000K:4500K

# Carefully start system tray apps
exec ~/bin/swaytray

# Almost all my key bindings are on Win
set $mod Mod4

# Several things run in terminals
set $term foot

### Input configuration
input type:keyboard {
    xkb_layout "us"
    xkb_variant "altgr-intl"
    xkb_options "ctrl:nocaps,pence_altgr_tweaks:on"
}

input type:touchpad {
    click_method clickfinger
    tap_button_map lrm
}

seat seat0 xcursor_theme elementary 24

focus_follows_mouse no
mouse_warping none

# Application hotkeys
bindsym $mod+t exec $term
bindsym $mod+e exec emacsclient -c -a ''
bindsym $mod+b exec firefox
bindsym $mod+z exec zotero
bindsym $mod+k exec keepassxc

# Window dragging, menu, and Alt+F4 are the only things on Alt
floating_modifier Mod1
bindsym Mod1+F2 exec wofi --show run --width 500 --height 500
bindsym Mod1+F4 kill

# Reload the configuration file
bindsym $mod+Shift+c reload
# End session
bindsym $mod+Shift+e exec ~/bin/end-session
# Lock screen
bindsym $mod+l exec swaylock -F -c 555577
# Print screen
bindsym $mod+Print exec grim -g "$(slurp)"

# Media hotkeys
bindsym XF86AudioPlay exec exaile --play-pause
bindsym XF86AudioStop exec exaile --stop
bindsym XF86AudioNext exec exaile --next
bindsym XF86AudioPrev exec exaile --prev
bindsym XF86AudioLowerVolume exec ~/bin/volume down
bindsym XF86AudioRaiseVolume exec ~/bin/volume up
bindsym XF86AudioMute exec ~/bin/volume toggle
bindsym XF86AudioMicMute exec ~/bin/micmute
bindsym XF86MonBrightnessUp exec ~/bin/brightness up
bindsym XF86MonBrightnessDown exec ~/bin/brightness down

# Dunst hotkeys
bindsym Mod4+Escape exec dunstctl close
bindsym Mod4+Shift+Escape exec dunstctl close-all
bindsym Mod4+grave exec dunstctl history-pop
bindsym Mod4+Shift+grave exec dunstctl context

# Change location of focus
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# Move windows around workspaces
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# Splitting
bindsym $mod+h split h
bindsym $mod+v split v

# Moving around outputs and fullscreening
bindsym $mod+Shift+Return move workspace to output right
bindsym $mod+Return fullscreen toggle
bindsym $mod+Shift+space floating toggle
bindsym $mod+space focus mode_toggle

# switch to workspace
bindsym $mod+F1 workspace 1
bindsym $mod+F2 workspace 2
bindsym $mod+F3 workspace 3
bindsym $mod+F4 workspace 4
bindsym $mod+F5 workspace 5
bindsym $mod+F6 workspace 6
bindsym $mod+F7 workspace 7
bindsym $mod+F8 workspace 8
bindsym $mod+F9 workspace 9
bindsym $mod+F10 workspace 10
bindsym $mod+F11 workspace 11
bindsym $mod+F12 workspace 12

# move focused container to workspace
bindsym $mod+Shift+F1 move container to workspace 1
bindsym $mod+Shift+F2 move container to workspace 2
bindsym $mod+Shift+F3 move container to workspace 3
bindsym $mod+Shift+F4 move container to workspace 4
bindsym $mod+Shift+F5 move container to workspace 5
bindsym $mod+Shift+F6 move container to workspace 6
bindsym $mod+Shift+F7 move container to workspace 7
bindsym $mod+Shift+F8 move container to workspace 8
bindsym $mod+Shift+F9 move container to workspace 9
bindsym $mod+Shift+F10 move container to workspace 10
bindsym $mod+Shift+F11 move container to workspace 11
bindsym $mod+Shift+F12 move container to workspace 12

# resize window
mode "resize" {
  bindsym Left resize shrink width 10 px or 10 ppt
  bindsym Down resize grow height 10 px or 10 ppt
  bindsym Up resize shrink height 10 px or 10 ppt
  bindsym Right resize grow width 10 px or 10 ppt

  bindsym Return mode "default"
  bindsym Escape mode "default"
  bindsym $mod+r mode "default"
}
bindsym $mod+r mode "resize"

# tea timer
mode "tea" {
  bindsym 1 exec $term -e termdown 1m; mode "default"
  bindsym 2 exec $term -e termdown 2m; mode "default"
  bindsym 3 exec $term -e termdown 3m; mode "default"
  bindsym 4 exec $term -e termdown 4m; mode "default"
  bindsym 5 exec $term -e termdown 5m; mode "default"
  bindsym 6 exec $term -e termdown 6m; mode "default"
  bindsym 7 exec $term -e termdown 7m; mode "default"
  bindsym 8 exec $term -e termdown 8m; mode "default"
  bindsym 9 exec $term -e termdown 9m; mode "default"
  bindsym 0 exec $term -e termdown 10m; mode "default"

  bindsym Return mode "default"
  bindsym Escape mode "default"
  bindsym $mod+period mode "default"
}
bindsym $mod+period mode "tea"

### Visual configuration
# <border> <background> <text> <split indicator> <child border>
client.focused          #3f3f3f #3f3f3f #dcdccc #8cd0d3 #656555
client.focused_inactive #3f3f3f #3f3f3f #dcdccc #3f3f3f #3f3f3f
client.unfocused        #3f3f3f #3f3f3f #dcdccc #3f3f3f #3f3f3f
client.urgent           #3f3f3f #3f3f3f #dcdccc #cc9393 #cc9393

default_border pixel 3
default_floating_border pixel 3
hide_edge_borders smart

# Special window configuration
for_window [window_type="dialog"] floating enable
for_window [window_role="dialog"] floating enable
for_window [title="Select one or more files to open"] floating enable

# Zotero has a few windows for citations that need to force floating
for_window [class="Zotero" title="Quick Format Citation"] floating enable
for_window [class="Zotero" title="^Progress$"] floating enable

# Apps that should be floated entirely by default
for_window [app_id="com.nextcloud.desktopclient.nextcloud"] floating enable
for_window [app_id="galculator"] floating enable
