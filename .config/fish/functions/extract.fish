# Support is missing here but could be easily added for:
# - archives: cab, arj
# - flat-file: .lz, .lrz

function __do_extract
    set filename (path resolve $argv[1])
    set original $argv[2]
    set ext (string lower (path extension $filename))

    # Handle single-file flat compressed files, and then try to extract them
    # again (for .tar.gz, .tar.bz2, etc.)
    set single 1
    switch $ext
        case ".bz" ".bz2" ".tbz2" ".tbz"
            command bunzip2 -k $filename
        case ".gz" ".Z" ".tgz" ".taz"
            command gunzip -k $filename
        case ".xz" ".txz"
            command unxz -k $filename
        case ".lzma" ".lz" ".tlz"
            command unlzma -k $filename
        case ".zst" ".zstd"
            command unzstd -k $filename
        case '*'
            set single 0
    end

    if test $single -eq 1
        set newfile (path change-extension '' $filename)
        __do_extract $newfile 0
    else
        # See if this is a multi-file compressed archive
        switch $ext
            case ".tar"
                set count (tar -tf $filename | wc -l)
            case ".zip"
                set count (zipinfo -1 $filename | wc -l)
            case ".7z"
                # FIXME: I think 7z l is printing garbage too, check!
                set count (7z l $filename | wc -l)
            case ".rar"
                # FIXME: I think unrar v is printing garbage too, check!
                set count (unrar v $filename | wc -l)
            case "*"
                # This is not a file we can extract, so abort entirely
                return
        end

        if test $count -gt 1
            # More than one file in this archive, put it in a subdirectory so
            # that we don't pollute the current directory
            #
            # FIXME: Theoretically, we could check to see if actually this
            # archive is itself a packaged directory; right now we just put that
            # directory in yet another subdir...
            set newdir (path change-extension '' $filename)
            mkdir $newdir
            cd $newdir
        end

        switch $ext
            case ".tar"
                command tar -xf $filename
            case ".zip"
                command unzip -q -O UTF-8 $filename
            case ".7z"
                command 7z x $filename
            case ".rar"
                command unrar x $filename
        end
    end

    # Delete other intermediate files, but don't delete the very first file we
    # were asked to extract
    if test $original -eq 0
        rm $filename
    end
end

function extract
    set workingdir (pwd)
    __do_extract $argv[1] 1
    cd $workingdir
end
