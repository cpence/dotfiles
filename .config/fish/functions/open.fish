function open --description "Open file in default application"
    if not set -q argv[1]
        printf "%ls: Expected at least %d args, got only %d\n" open 1 0 >&2
        return 1
    end

    for i in $argv
        handlr open $i
    end
end
