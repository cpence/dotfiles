function ls
    # Only show indicators if we're not piped into a script
    isatty stdout && set -fa indicators --file-type

    /bin/ls $indicators --sort=version --color=auto $argv
end
