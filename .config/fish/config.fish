# ----------
# Fish configuration

# Theme configuration
set -g default_user cpence
fish_config theme choose "Catppuccin Latte"
set -g theme_powerline_fonts yes
set -g theme_nerd_fonts yes
set -g theme_display_vi no
set -g theme_display_hostname ssh
set -g theme_color_scheme catpuccin-macchiato
set -g theme_display_date no
set -g theme_display_screen no
# set -g theme_newline_cursor yes
set -g theme_vcs_ignore_paths /mnt

# ----------
# Paths
fish_add_path ~/.local/bin
fish_add_path ~/bin

# Go
fish_add_path ~/Development/go
set -gx GOPATH ~/Development/go
set -gx GO111MODULE on

# Python
set -gx NLTK_DATA ~/.cache/nltk-data
set -gx GENSIM_DATA_DIR ~/.cache/gensim-data
set -gx SCIKIT_LEARN_DATA ~/.cache/scikit-learn

# Tell remote borg where to find itself on rsync.net
set -gx BORG_REMOTE_PATH /usr/local/bin/borg_1.2.8/borg1

# ----------
# Environment variables

# Locale configuration
set -gx LANG en_US.UTF-8

# Basic program configuration
set -gx EDITOR /usr/bin/nano
set -gx VISUAL /usr/bin/nano
set -gx BROWSER /usr/bin/firefox

# Video acceleration
set -gx LIBVA_DRIVER_NAME iHD
set -gx VDPAU_DRIVER va_gl

# Miscellaneous GUI settings
set -gx GTK_THEME Mac-OS-9
set -gx GTK_OVERLAY_SCROLLING 0

set -gx QT_QPA_PLATFORM wayland
set -gx QT_QPA_PLATFORMTHEME qt5ct

# LibreOffice
set -gx SAL_USE_VCPLUGIN gtk3

# Java
set -gx _JAVA_OPTIONS "-Dswing.crossplatformlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel -Dawt.useSystemAAFontSettings=lcd -Dswing.aatext=true"
# tiling WM only
# set -gx _JAVA_AWT_WM_NONREPARENTING 1

# Others
set -gx LESSHISTFILE -
set -gx TEXMFCNF "$HOME/bin/texmf/cnf/:"

# ----------
# Interactive sessions
if status is-interactive
    # Handle all dev-tools and environments with mise
    set -gx MISE_IGNORED_CONFIG_PATHS /mnt/
    mise activate fish | source

    # Security and crypto only in interactive sessions
    set -gx GPG_TTY (tty)
    set -gx SSH_AUTH_SOCK (gpgconf --list-dirs agent-ssh-socket)
    set -g KEYID 0x1A6417A51A895544
    gpgconf --launch gpg-agent

    # Always run screen (or resume it), unless we were told not to or we're in
    # the Linux framebuffer console
    if test -z "$NO_SCREEN" -a -z "$STY" -a $TERM != linux
        exec screen -xRRq
    end
end
