$pdf_mode = 4;
$force_mode = 1;

$latex = "latex -interaction=nonstopmode -halt-on-error";
$pdflatex = "pdflatex -interaction=nonstopmode -halt-on-error";
$lualatex = "lualatex -interaction=nonstopmode -halt-on-error";
$xelatex = "xelatex -interaction=nonstopmode -halt-on-error";

$pdf_previewer = "start atril %O %S";

$cleanup_includes_generated = 1;
$cleanup_includes_cusdep_generated = 1;
$clean_ext = "synctex.gz %R-blx.bib run.xml snm nav pyg ist xdy fls glg glo log out";
